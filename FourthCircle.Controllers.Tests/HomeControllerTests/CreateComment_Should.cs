﻿using System;
using System.Collections.Generic;
using FourthCircle.Data.Entities;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.HomeControllerTests
{
    [TestClass]
    public class CreateComment_Should
    {
        [TestMethod]
        public void Return_ViewOnGetRequest()
        {
            var mapper = new CommentViewModelMapper();
            //var fakeComments = new List<Comment> { new Comment("author", "text", new Location("location"), false) };
            var visitorService = new Mock<ICommentService>();
            //visitorService.Setup(x => x.GetAllComments()).Returns(fakeComments).Verifiable();
            var locationService = new Mock<ILocationService>();
            locationService.Setup(x=>x.GetLocationNames()).Returns(It.IsAny<ICollection<string>>());
            var controller = new HomeController(mapper, visitorService.Object, locationService.Object);

            var sut = controller.CreateComment();

            Assert.IsInstanceOfType(sut, typeof(PartialViewResult));
            locationService.Verify(x => x.GetLocationNames(), Times.Once);
        }

        [TestMethod]
        public void Return_View_OnPostRequest_WhenModelIsNotValid()
        {
            var mapper = new CommentViewModelMapper();
            var visitorService = new Mock<ICommentService>();
            var locationService = new Mock<ILocationService>();

            var controller = new HomeController(mapper, visitorService.Object, locationService.Object);
            controller.ModelState.AddModelError("Text","Required");
            var fakeCommentViewModel = new CommentViewModel();

            var sut = controller.CreateComment(fakeCommentViewModel);

            Assert.IsInstanceOfType(sut, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void ReturnView_OnSuccess()
        {
            var fakeComment = new Comment("author", "text", new Location("location"), false);

            var mapper = new CommentViewModelMapper();
            var visitorService = new Mock<ICommentService>();
            visitorService.Setup(x=>x.CreateComment(It.IsAny<string>(),It.IsAny<string>(),It.IsAny<bool?>(),It.IsAny<string>())).Returns(fakeComment).Verifiable();
            var locationService = new Mock<ILocationService>();
            var controller = new HomeController(mapper, visitorService.Object, locationService.Object);

            var fakeCommentViewModel = new CommentViewModel();

            var sut = controller.CreateComment(fakeCommentViewModel);

            Assert.IsInstanceOfType(sut, typeof(PartialViewResult));
            visitorService.Verify(x=>x.CreateComment(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool?>(), "Anonymous"),Times.Once);
        }
    }
}
