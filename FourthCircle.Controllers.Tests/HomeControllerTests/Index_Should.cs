﻿using System.Linq;
using System.Collections.Generic;
using FourthCircle.Data.Entities;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Threading.Tasks;

namespace FourthCircle.Controllers.Tests.HomeControllerTests
{
    [TestClass]
    public class Index_Should
    {
        [TestMethod]
        public async Task Return_View()
        {
            var mapper = new CommentViewModelMapper();
            var fakeComments = new List<Comment> { new Comment("author", "text", new Location("location"), false) }.AsQueryable();
            var visitorService = new Mock<ICommentService>();
            visitorService.Setup(x => x.GetAllComments()).Returns(fakeComments).Verifiable();
            var locationService = new Mock<ILocationService>();

            var controller = new HomeController(mapper, visitorService.Object, locationService.Object);

            var sut = await controller.Index(1);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
            visitorService.Verify(x => x.GetAllComments(), Times.Once);
        }
    }
}
