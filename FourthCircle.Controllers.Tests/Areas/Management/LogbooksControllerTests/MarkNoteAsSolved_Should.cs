﻿using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Microsoft.AspNetCore.Mvc;
using FourthCircle.Mappers;
using FourthCircle.Controllers.Tests.Areas.Administrative;
using FourthCircle.Data;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace FourthCircle.Controllers.Tests.Areas.Management.LogbooksControllerTests
{
    [TestClass]
    public class MarkNoteAsSolved_Should
    {
        [TestMethod]
        public void ReturnViewResult_WhenIdIsValid()
        {
            var mockContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockContext.Object);
            var managerService = new Mock<IManagerService>();
            var noteMapper = new NoteViewModelMapper();

            var fakeUser = new ApplicationUser("testUser");

            var userId = fakeUser.Id;

            var fakeLogbook = new Logbook("fakeLogbook");
            var logbookId = fakeLogbook.Id;
            var noteText = "noteTxt";

            var fakeNote = new Note(fakeUser, noteText, fakeLogbook);
            var noteId = fakeNote.Id;

            managerService.Setup(x => x.MarkNoteAsSolved(noteId, logbookId, userId)).Returns(fakeNote);

            var claim = new Claim("fakeUserName", "fakeUserId");
            var mockIdentity =
                Mock.Of<ClaimsPrincipal>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
            var mockControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = mockIdentity
                }
            };

            var controller = new FourthCircle.Areas.Management.Controllers.LogbooksController(
                null, null, noteMapper, managerService.Object, userManager, null, null, null, null)
            {
                ControllerContext = mockControllerContext
            };

            var sut = controller.MarkNoteAsSolved(noteId, logbookId, null, null, null, null, null, It.IsAny<bool>());

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
        }

        [TestMethod]
        public void ReturnBadRequestResult_WhenIdIsNotValid()
        {
            var managerService = new Mock<IManagerService>();
            var noteMapper = new NoteViewModelMapper();

            var logbookId = "fakeLogbookId";
            var noteId = "fakeNoteId";

            var fakeUser = new ApplicationUser("username");
            var claim = new Claim("fakeUserName", "fakeUserId");
            var mockIdentity =
                Mock.Of<ClaimsPrincipal>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
            var mockControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = mockIdentity
                }
            };

            var fakeUserId = fakeUser.Id;
            Note fakeNote = null;

            var mockDbContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockDbContext.Object);

            managerService.Setup(x => x.MarkNoteAsSolved(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new System.ArgumentException());

            var controller = new FourthCircle.Areas.Management.Controllers.LogbooksController(
                null, null, noteMapper, managerService.Object, userManager, null, null, null, null)
            {
                ControllerContext = mockControllerContext
            };

            var sut = controller.MarkNoteAsSolved(noteId, logbookId, null, null, null, null, null, It.IsAny<bool>());

            Assert.IsInstanceOfType(sut, typeof(BadRequestResult));
        }
    }
}
