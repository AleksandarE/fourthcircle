﻿using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Microsoft.AspNetCore.Mvc;
using FourthCircle.Mappers;
using FourthCircle.Areas.Management.Controllers;
using FourthCircle.Controllers.Tests.Areas.Administrative;
using System.Security.Claims;
using FourthCircle.Data;
using Microsoft.AspNetCore.Http;

namespace FourthCircle.Controllers.Tests.Areas.Management.LogbooksControllerTests
{
    [TestClass]
    public class DeleteNote_Should
    {
        [TestMethod]
        public void DeleteNote_AndRedirect_WhenIdIsValid()
        {
            var fakeUser = new ApplicationUser("username");

            var claim = new Claim("fakeUserName", "fakeUserId");
            var mockIdentity =
                Mock.Of<ClaimsPrincipal>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
            var mockControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = mockIdentity
                }
            };

            var mockDbContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockDbContext.Object);

            var mapper = new NoteViewModelMapper();
            var managerService = new Mock<IManagerService>();
            var fakeLogbook = new Logbook("logbookName");
            var fakeLogbookId = fakeLogbook.Id;
            var fakeNoteText = "test";
            var fakeNote = new Note(fakeUser, fakeNoteText, fakeLogbook, null);
            var userId = fakeUser.Id;
            var fakeNoteId = fakeNote.Id;

            var controller = new LogbooksController(
                null, null, mapper, managerService.Object, userManager, null, null, null, null)
            {
                ControllerContext = mockControllerContext
            };

            var sut = controller.DeleteNote(fakeLogbookId, fakeNoteId);

            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
        }

        [TestMethod]
        public void ReturnBadRequestResult_WhenDeleteMethodThrows()
        {
            var fakeUser = new ApplicationUser("username");

            var claim = new Claim("fakeUserName", "fakeUserId");
            var mockIdentity =
                Mock.Of<ClaimsPrincipal>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
            var mockControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = mockIdentity
                }
            };

            var mockDbContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockDbContext.Object);

            var mapper = new NoteViewModelMapper();
            var managerService = new Mock<IManagerService>();
            var fakeUserId = fakeUser.Id;
            var fakeLogbookId = "testLogbookId";
            var fakeNoteId = "testNoteId";
            managerService.Setup(s => s.DeleteNote(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new System.ArgumentException());

            var controller = new LogbooksController(
                null, null, mapper, managerService.Object, userManager, null, null, null, null)
            {
                ControllerContext = mockControllerContext
            };

            var sut = controller.DeleteNote(fakeLogbookId, fakeNoteId);

            Assert.IsInstanceOfType(sut, typeof(BadRequestResult));
        }

    //    [TestMethod]
    //    public void Throw_IfUserIdIsNull()
    //    {

    //        var mockDbContext = new Mock<ApplicationDbContext>();
    //        var userManager = HelperTestMethods.GetUserManager(mockDbContext.Object);

    //        var mapper = new NoteViewModelMapper();
    //        var managerService = new Mock<IManagerService>();

    //        var controller = new LogbooksController(
    //            null, null, mapper, managerService.Object, userManager);
    //        var fakeLogbookId = "fakeLogbookId";
    //        var fakeNoteId = "fakeNoteId";
    //        string userId = null;

    //        //var sut = controller.DeleteNote(fakeLogbookId, fakeNoteId);

    //        //Assert.IsInstanceOfType(sut, typeof(BadRequestResult));
    //        Assert.ThrowsException<System.ApplicationException>(() => controller.DeleteNote(fakeLogbookId, fakeNoteId),
    //$"Unable to load user with ID '{userId}'.");

    //    }
    }
}
