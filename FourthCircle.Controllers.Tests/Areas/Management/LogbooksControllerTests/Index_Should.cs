﻿using System.Collections.Generic;
using System.Linq;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Microsoft.AspNetCore.Mvc;
using FourthCircle.Mappers;
using FourthCircle.Controllers.Tests.Areas.Administrative;
using FourthCircle.Data;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using FourthCircle.Areas.Management.Controllers;

namespace FourthCircle.Controllers.Tests.Areas.Management.LogbooksControllerTests
{
    [TestClass]
    public class Index_Should
    {
        [TestMethod]
        public void Return_ViewResult()
        {
            var locationService = new Mock<ILocationService>();
            var locationMapper = new LocationViewModelMapper();

            IQueryable<Location> fakeLocations = new List<Location> { new Location("fakeLocation")} as IQueryable<Location>;

            var fakeUser = new ApplicationUser("username");
            var claim = new Claim("fakeUserName", "fakeUserId");
            var mockIdentity =
                Mock.Of<ClaimsPrincipal>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
            var mockControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = mockIdentity
                }
            };

            var userId = fakeUser.Id;

            locationService.Setup(x => x.GetLocationsByUser(userId)).Returns(fakeLocations);

            var mockDbContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockDbContext.Object);

            var controller = new LogbooksController(
                null, null, null, null, userManager, locationService.Object, locationMapper, null, null)
            {
                ControllerContext = mockControllerContext
            };

            var sut = controller.Index();

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
            locationService.Verify();
        }
    }
}
