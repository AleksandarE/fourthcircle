﻿using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Microsoft.AspNetCore.Mvc;
using FourthCircle.Mappers;
using FourthCircle.Areas.Management.Controllers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Controllers.Tests.Areas.Administrative;
using System.Security.Claims;
using FourthCircle.Data;
using Microsoft.AspNetCore.Http;

namespace FourthCircle.Controllers.Tests.Areas.Management.LogbooksControllerTests
{
    [TestClass]
    public class EditNote_Should
    {
        [TestMethod]
        public void Return_ViewOnHTTPGet()
        {
            var managerService = new Mock<IManagerService>();
            var noteMapper = new NoteViewModelMapper();
            var fakeLogbook = new Logbook("fakeLogbook");
            var fakeLogbookId = fakeLogbook.Id;
            var fakeText = "fakeNoteText";
            var fakeNote = new Note
            {
                LogbookId = fakeLogbookId,
                Text = fakeText
            };

            var fakeNoteId = fakeNote.Id;

            var fakeUser = new ApplicationUser("username");
            var claim = new Claim("fakeUserName", "fakeUserId");
            var mockIdentity =
                Mock.Of<ClaimsPrincipal>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
            var mockControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = mockIdentity
                }
            };

            var mockDbContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockDbContext.Object);

            managerService.Setup(x => x.GetNoteById(fakeNoteId)).Returns(fakeNote).Verifiable();

            var controller = new LogbooksController(null, null, noteMapper, managerService.Object, userManager, null, null, null, null)
            {
                ControllerContext = mockControllerContext
            };

            var sut = controller.EditNote(fakeLogbookId, fakeNoteId);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
        }

        [TestMethod]
        public void ReturnViewOnPostRequest_WhenModelIsNotValid()
        {
            var mapper = new NoteViewModelMapper();
            var managerService = new Mock<IManagerService>();

            var controller = new LogbooksController(null, null, mapper, managerService.Object, null, null, null, null, null);
            controller.ModelState.AddModelError("Text", "Required");
            var fakeNoteViewModel = new NoteViewModel();

            var sut = controller.EditNote(fakeNoteViewModel);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
        }

        [TestMethod]
        public void EditNoteAndRedirect_WhenModelIsValid()
        {
            var mapper = new NoteViewModelMapper();
            var managerService = new Mock<IManagerService>();
            var user = new ApplicationUser("username");
            var fakeLogbook = new Logbook("logbookName");
            var fakeLocationId = fakeLogbook.Id;
            var fakeNoteText = "test";
            var userId = user.Id;
            managerService.Setup(x => x.EditNote(userId, fakeNoteText, fakeLocationId, null, null)).Returns(new Note(user, fakeNoteText, fakeLogbook, null));//.Verifiable();

            var fakeUser = new ApplicationUser("username");
            var claim = new Claim("fakeUserName", "fakeUserId");
            var mockIdentity =
                Mock.Of<ClaimsPrincipal>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
            var mockControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = mockIdentity
                }
            };

            var mockDbContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockDbContext.Object);

            var controller = new LogbooksController(
                null, null, mapper, managerService.Object, userManager, null, null, null, null)
            {
                ControllerContext = mockControllerContext
            };

            var fakeViewModel = new NoteViewModel() { Text = fakeNoteText, Logbook = fakeLogbook, UserID = userId};

            var sut = controller.EditNote(fakeViewModel);

            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
            //managerService.Verify();
        }
    }
}
