﻿using System.Collections.Generic;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Microsoft.AspNetCore.Mvc;
using FourthCircle.Mappers;

namespace FourthCircle.Controllers.Tests.Areas.Management.LogbooksControllerTests
{
    [TestClass]
    public class Notes_Should
    {
        [TestMethod]
        public void ReturnViewResult_WhenIdIsValid()
        {
            var managerService = new Mock<IManagerService>();
            var noteMapper = new NoteViewModelMapper();
            var fakeUser = new ApplicationUser("fakeUser");
            var fakeLogbook = new Logbook("fakeLogbook");
            var logbookId = fakeLogbook.Id;
            var noteText = "noteTxt";

            var fakeNotes = new List<Note> { new Note(fakeUser, noteText, fakeLogbook) };

            managerService.Setup(x => x.FilterLogbookNotes(logbookId, null, null, null, null)).Returns(fakeNotes).Verifiable();

            var controller = new FourthCircle.Areas.Management.Controllers.LogbooksController(
                null, null, noteMapper, managerService.Object, null, null, null, null, null);

            var sut = controller.Notes(logbookId, null, null, null, null, null,It.IsAny<bool>());

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
            managerService.Verify();
        }

        [TestMethod]
        public void ReturnBadRequestResult_WhenIdIsNotValid()
        {
            var managerService = new Mock<IManagerService>();
            var noteMapper = new NoteViewModelMapper();
            var fakeUser = new ApplicationUser("fakeUser");
            var fakeLogbook = new Logbook("fakeLogbook");
            var logbookId = "test";

            ICollection<Note> fakeNotes = null;

            managerService.Setup(x => x.FilterLogbookNotes(logbookId, null, null, null, null)).Returns(fakeNotes).Verifiable();

            var controller = new FourthCircle.Areas.Management.Controllers.LogbooksController(
                null, null, noteMapper, managerService.Object, null, null, null, null, null);

            var sut = controller.Notes(logbookId, null, null, null, null, null,It.IsAny<bool>());

            Assert.IsInstanceOfType(sut, typeof(BadRequestResult));
            managerService.Verify();
        }
    }
}
