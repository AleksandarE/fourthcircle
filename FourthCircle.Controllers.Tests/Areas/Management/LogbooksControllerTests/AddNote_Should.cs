﻿using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Microsoft.AspNetCore.Mvc;
using FourthCircle.Mappers;
using FourthCircle.Areas.Management.Controllers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Controllers.Tests.Areas.Administrative;
using System.Security.Claims;
using FourthCircle.Data;
using Microsoft.AspNetCore.Http;

namespace FourthCircle.Controllers.Tests.Areas.Management.LogbooksControllerTests
{
    [TestClass]
    public class AddNote_Should
    {
        //[TestMethod]
        //public void Return_ViewOnHTTPGet()
        //{
        //    var fakeUser = new ApplicationUser("username");

        //    var claim = new Claim("fakeUserName", "fakeUserId");
        //    var mockIdentity =
        //        Mock.Of<ClaimsPrincipal>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
        //    var mockControllerContext = new ControllerContext
        //    {
        //        HttpContext = new DefaultHttpContext
        //        {
        //            User = mockIdentity
        //        }
        //    };
        //    var mockDbContext = new Mock<ApplicationDbContext>();
        //    var userManager = HelperTestMethods.GetUserManager(mockDbContext.Object);

        //    var managerService = new Mock<IManagerService>();
        //    var fakeCategories = new List<Category>() { new Category("1"), new Category("2") };
        //    var fakeCategoriesAsString = new List<string>() { "1", "2" };
        //    var noteMapper = new NoteViewModelMapper();
        //    var fakeLogbook = new Logbook("fakeLogbook");
        //    var logbookModel = new LogbookViewModel { Id = fakeLogbook.Id };
        //    var logbookId = fakeLogbook.Id;

        //    managerService.Setup(x => x.GetCategoryListAsString(logbookId)).Returns(fakeCategoriesAsString);

        //    var controller = new LogbooksController(null, null, noteMapper, managerService.Object, userManager)
        //    {
        //        ControllerContext = mockControllerContext
        //    };

        //    var sut = controller.AddNote(logbookModel);

        //    Assert.IsInstanceOfType(sut, typeof(ViewResult));
        //}

        [TestMethod]
        public void ReturnViewOnPostRequest_WhenModelIsNotValid()
        {
            var mapper = new NoteViewModelMapper();
            var managerService = new Mock<IManagerService>();

            var controller = new LogbooksController(null, null, mapper, managerService.Object, null, null, null, null, null);
            controller.ModelState.AddModelError("Text", "Required");
            var fakeNoteViewModel = new NoteViewModel();

            var sut = controller.AddNoteAsync(fakeNoteViewModel);

            Assert.IsInstanceOfType(sut, typeof(System.Threading.Tasks.Task));
        }

        [TestMethod]
        public void AddNoteAndRedirect_WhenModelIsValid()
        {
            var fakeUser = new ApplicationUser("username");

            var claim = new Claim("fakeUserName", "fakeUserId");
            var mockIdentity =
                Mock.Of<ClaimsPrincipal>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
            var mockControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext
                {
                    User = mockIdentity
                }
            };
            var mockDbContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockDbContext.Object);

            var mapper = new NoteViewModelMapper();
            var managerService = new Mock<IManagerService>();
            var fakeLogbook = new Logbook("logbookName");
            var fakeLocationId = fakeLogbook.Id;
            var fakeNoteText = "test";
            var userId = fakeUser.Id;

            managerService.Setup(x => x.AddNote(userId, fakeNoteText, fakeLocationId, null, null)).Returns(new Note(fakeUser, fakeNoteText, fakeLogbook, null));//.Verifiable();

            var controller = new LogbooksController(
                null, null, mapper, managerService.Object, userManager, null, null, null, null)
            {
                ControllerContext = mockControllerContext
            };

            var fakeViewModel = new NoteViewModel
            {
                UserID = fakeUser.Id,
                Text = fakeNoteText,
                Logbook = fakeLogbook
            };

            var sut = controller.AddNoteAsync(fakeViewModel);

            Assert.IsInstanceOfType(sut, typeof(System.Threading.Tasks.Task));
            //managerService.Verify();
        }
    }
}
