﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FourthCircle.Controllers.Tests.Areas.Administrative
{
    public static class HelperTestMethods
    {
        public static async Task<ICollection<string>> GetFakeUsers()
        {
            var fakeUsers = new List<string> { "Joe", "John" }.ToList();
            return fakeUsers;
        }

        public static IQueryable<ApplicationUser> GetUsersAsQueriable()
        {
            var fakeUsers = new List<ApplicationUser> { new ApplicationUser("Joe"), new ApplicationUser("John") }.AsQueryable();
            return fakeUsers;
        }

        public static UserManager<ApplicationUser> GetUserManager(ApplicationDbContext context)
        {
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store, null, null, null, null, null, null, null, null);
            return userManager;
        }
        public static RoleManager<IdentityRole> GetRoleManager(ApplicationDbContext context)
        {
            var store = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(store, null, null, null, null);
            return roleManager;
        }
    }
}
