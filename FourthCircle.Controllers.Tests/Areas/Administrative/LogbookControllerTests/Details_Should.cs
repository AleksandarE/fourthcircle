﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.LogbookControllerTests
{
    [TestClass]
    public class Details_Should
    {
        [TestMethod]
        public async Task Return_BadRequestResult_WhenIdIsNotValid()
        {
            string fakeId = "test";
            Logbook fakeLogbook = null;

            var logbookService = new Mock<ILogbookService>();
            logbookService.Setup(x=>x.GetLogbook(fakeId)).Returns(fakeLogbook);
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<IUserService>();
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);

            var sut = await controller.Details(fakeId);

            Assert.IsInstanceOfType(sut, typeof(BadRequestResult));
        }

        [TestMethod]
        public async Task Return_ViewModel_WhenIdIsValid()
        {
            string fakeId = "test";
            Logbook fakeLogbook = new Logbook("logbook") { Location = new Location("locations"),Id=fakeId};

            var logbookService = new Mock<ILogbookService>();
            logbookService.Setup(x => x.GetLogbook(fakeId)).Returns(fakeLogbook);
            var mapper = new LogbookViewModelMapper();
            var categoryService = new Mock<ICategoryService>();
            var fakeCategories = new List<Category>() { new Category("1"), new Category("2") };
            categoryService.Setup(x => x.GetAllCategories()).Returns(fakeCategories);
            var userService = new Mock<IUserService>();
            userService.Setup(x => x.GetUsernamesInRole("Manager")).Returns(HelperTestMethods.GetFakeUsers());
            var controller = new LogbookController(logbookService.Object, mapper, categoryService.Object, userService.Object);

            var sut = await controller.Details(fakeId);

            Assert.IsInstanceOfType(sut, typeof(PartialViewResult));
        }

    }
}
