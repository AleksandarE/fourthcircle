﻿using System;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.LogbookControllerTests
{
    [TestClass]
    public class CreateCategory_Should
    {
        [TestMethod]
        public void Return_PartialView_OnGet()
        {
            var logbookService = new Mock<ILogbookService>();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<IUserService>();

            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);

            var sut = controller.CreateCategory();

            Assert.IsInstanceOfType(sut, typeof(PartialViewResult));
        }

        [TestMethod]
        public void Return_ViewWhenModelIsNotValid()
        {
            var logbookService = new Mock<ILogbookService>();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<IUserService>();
            var fakeCategoryViewModel = new CategoryViewModel();
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);
            controller.ModelState.AddModelError("Name", "Required");

            var sut = controller.CreateCategory(fakeCategoryViewModel);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
        }

        [TestMethod]
        public void Return_ViewOnSuccess()
        {
            var categoryName = "test";
            var logbookService = new Mock<ILogbookService>();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<IUserService>();
            var fakeCategoryViewModel = new CategoryViewModel() { Name = categoryName };
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);

            var sut = controller.CreateCategory(fakeCategoryViewModel);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
        }

        [TestMethod]
        public void Return_ViewOnException()
        {
            var fakeException = "fake exception";
            var categoryName = "test";
            var logbookService = new Mock<ILogbookService>();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            categoryService.Setup(x=>x.CreateCategory(It.IsAny<string>())).Throws(new ArgumentException(fakeException));
            var userService = new Mock<IUserService>();
            var fakeCategoryViewModel = new CategoryViewModel() { Name = categoryName };
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);

            var sut = controller.CreateCategory(fakeCategoryViewModel);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
            Assert.IsTrue(controller.ModelState.ErrorCount==1);
        }
    }
}
