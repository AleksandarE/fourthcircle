﻿using System;
using System.Threading.Tasks;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.LogbookControllerTests
{
    [TestClass]
    public class DeleteLogbook_Should
    {
        [TestMethod]
        public void Return_ViewWhenModelIsNotValid()
        {
            var logbookService = new Mock<ILogbookService>();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<IUserService>();
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);
            var fakeViewModel = new LogbookViewModel() { LocationName = "location", AssignedManagers = null, Categories = null };
            controller.ModelState.AddModelError("Name", "Required");
            var sut = controller.DeleteLogbook(fakeViewModel);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
        }

        [TestMethod]
        public void Update_AndRedirect()
        {
            string fakeId = "test";
            Logbook fakeLogbook = new Logbook("logbook") { Location = new Location("locations"), Id = fakeId };
            var logbookService = new Mock<ILogbookService>();
            logbookService.Setup(x=>x.GetLogbook(fakeId)).Returns(fakeLogbook);
            logbookService.Setup(x => x.DeleteLogbook(fakeLogbook)).Verifiable();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<IUserService>();
            var fakeViewModel = new LogbookViewModel() { Id = fakeId, Name = "logbook", LocationName = "location", AssignedManagers = null, Categories = null };
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);

            var sut = controller.DeleteLogbook(fakeViewModel);

            logbookService.Verify(x => x.DeleteLogbook(fakeLogbook), Times.Once);
            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
        }
    }
}
