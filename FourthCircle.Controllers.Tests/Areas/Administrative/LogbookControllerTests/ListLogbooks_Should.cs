﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Data.Entities;
using FourthCircle.Services;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.LogbookControllerTests
{
    [TestClass]
    public class ListLogbooks_Should
    {
        [TestMethod]
        public async Task Return_ViewResult_WithListOfLogbooks()
        {
            var logbookService = new Mock<ILogbookService>();
            logbookService.Setup(x => x.FilterLogbooks(null, null, null)).Returns(GetTestLogbooks());
            var mapper = new LogbookViewModelMapper();

            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<UserService>();

            var controller = new LogbookController(logbookService.Object, mapper, categoryService.Object, userService.Object);

            var sut = await controller.ListLogbooks(null, null, null, null, null, null);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));

        }

        private IQueryable<Logbook> GetTestLogbooks()
        {
            var logbooks = new List<Logbook>()
            {
                new Logbook("LB1")
                        { 
                        Location = new Location("location"),
                        Id = "testId"
                        },
                new Logbook("LB2")
                        { 
                        Location = new Location("location"),
                        Id = "testId"
                        }
            };
            var queryableLogbooks = logbooks.AsQueryable();
            return queryableLogbooks;
        }
    }
}
