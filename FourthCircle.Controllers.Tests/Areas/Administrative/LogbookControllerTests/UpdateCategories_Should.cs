﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.LogbookControllerTests
{
    [TestClass]
    public class UpdateCategories_Should
    {
        [TestMethod]
        public void Return_ViewWhenModelIsNotValid()
        {
            var logbookService = new Mock<ILogbookService>();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<IUserService>();
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);
            var fakeViewModel = new LogbookViewModel() { LocationName = "location", AssignedManagers = null, Categories = GetFakeCategories() };
            controller.ModelState.AddModelError("Name", "Required");
            var sut = controller.UpdateCategories(fakeViewModel);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
        }

        [TestMethod]
        public void Update_AndRedirect()
        {
            string fakeId = "test";
            Logbook fakeLogbook = new Logbook("logbook") { Location = new Location("locations"), Id = fakeId };
            var fakeCategories = GetFakeCategories();
            var logbookService = new Mock<ILogbookService>();
            logbookService.Setup(x => x.AssignCategories(fakeId, fakeCategories)).Returns(fakeLogbook).Verifiable();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<IUserService>();
            var fakeViewModel = new LogbookViewModel() { Id = fakeId, Name = "logbook", LocationName = "location", AssignedManagers = null, Categories = fakeCategories };
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);

            var sut = controller.UpdateCategories(fakeViewModel);

            logbookService.Verify(x=>x.AssignCategories(fakeId,fakeCategories),Times.Once);
            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
        }

        private ICollection<string> GetFakeCategories()
        {
            var fakeCategories = new List<string>() { "1", "2" };
            return fakeCategories;
        }
    }
}
