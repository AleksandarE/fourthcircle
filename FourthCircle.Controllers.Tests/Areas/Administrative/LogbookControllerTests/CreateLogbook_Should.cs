﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FourthCircle.Controllers.Tests.Areas.Administrative;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.LogbookControllerTests
{
    [TestClass]
    public class CreateLogbook_Should
    {
        [TestMethod]
        public async Task Return_ViewOnHTTPGet()
        {
            var logbookService = new Mock<ILogbookService>();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var fakeCategories = new List<Category>() { new Category("1"),new Category("2")};
            categoryService.Setup(x=>x.GetAllCategories()).Returns(fakeCategories);
            var userService = new Mock<IUserService>();

            userService.Setup(x=>x.GetUsernamesInRole("Manager")).Returns(HelperTestMethods.GetFakeUsers());

            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);

            var sut = await controller.CreateLogbook();

            Assert.IsInstanceOfType(sut, typeof(PartialViewResult));
        }

        [TestMethod]
        public async Task Return_ViewWhenModelIsNotValid()
        {
            var logbookService = new Mock<ILogbookService>();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var fakeCategories = new List<Category>() { new Category("1"), new Category("2") };
            var fakeLogbookModel = new LogbookViewModel() { Name= "fakeLogbook",LocationName = "fake location"};
            categoryService.Setup(x => x.GetAllCategories()).Returns(fakeCategories);
            var userService = new Mock<IUserService>();
            userService.Setup(x => x.GetUsernamesInRole("Manager")).Returns(HelperTestMethods.GetFakeUsers());
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);
            controller.ModelState.AddModelError("Name","Required");

            var sut = await controller.CreateLogbook(fakeLogbookModel);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
        }

        [TestMethod]
        public async Task CreateLogbook_AndReturnView()
        {
            var logbookService = new Mock<ILogbookService>();
            logbookService.Setup(x=>x.CreateLogbook("name","location",null,null)).Returns(new Logbook("name") { Location = new Location("location")}).Verifiable();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var fakeCategories = new List<Category>() { new Category("1"), new Category("2") };
            categoryService.Setup(x => x.GetAllCategories()).Returns(fakeCategories);
            var userService = new Mock<IUserService>();
            userService.Setup(x => x.GetUsernamesInRole("Manager")).Returns(HelperTestMethods.GetFakeUsers());
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);
            var fakeViewModel = new LogbookViewModel() { Name = "name", LocationName="location",AssignedManagers = null, Categories = null};

            var sut = await controller.CreateLogbook(fakeViewModel);

            Assert.IsInstanceOfType(sut,(typeof(ViewResult)));
            logbookService.Verify();
        }

        [TestMethod]
        public async Task ReturnView_InCaseOfException()
        {
            var logbookService = new Mock<ILogbookService>();
            logbookService.Setup(x => x.CreateLogbook(It.IsAny<string>(), It.IsAny<string>(), null, null)).Throws(new ArgumentException()).Verifiable();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var fakeCategories = new List<Category>() { new Category("1"), new Category("2") };
            categoryService.Setup(x => x.GetAllCategories()).Returns(fakeCategories);
            var userService = new Mock<IUserService>();
            userService.Setup(x => x.GetUsernamesInRole("Manager")).Returns(HelperTestMethods.GetFakeUsers());
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);
            var fakeViewModel = new LogbookViewModel() { Name = "name", LocationName = "location", AssignedManagers = null, Categories = null };

            var sut = await controller.CreateLogbook(fakeViewModel);

            Assert.IsInstanceOfType(sut, (typeof(ViewResult)));
            Assert.IsTrue(controller.ModelState.ErrorCount == 1);
            logbookService.Verify();
        }


    }
}
