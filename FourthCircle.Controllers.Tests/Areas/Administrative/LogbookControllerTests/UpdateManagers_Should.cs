﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.LogbookControllerTests
{
    [TestClass]
    public class UpdateManagers_Should
    {
        [TestMethod]
        public void Return_ViewWhenModelIsNotValid()
        {
            var logbookService = new Mock<ILogbookService>();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<IUserService>();
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);
            var fakeViewModel = new LogbookViewModel() { LocationName = "location", AssignedManagers = null, Categories = null };
            controller.ModelState.AddModelError("Name", "Required");
            var sut = controller.UpdateManagers(fakeViewModel);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
        }

        [TestMethod]
        public async Task Update_AndRedirect()
        {
            string fakeId = "test";
            Logbook fakeLogbook = new Logbook("logbook") { Location = new Location("locations"), Id = fakeId };
            var fakeManagers = await HelperTestMethods.GetFakeUsers();
            var logbookService = new Mock<ILogbookService>();
            logbookService.Setup(x => x.AssignManagers(fakeId, fakeManagers)).Returns(fakeLogbook).Verifiable();
            var mapper = new Mock<LogbookViewModelMapper>();
            var categoryService = new Mock<ICategoryService>();
            var userService = new Mock<IUserService>();
            var fakeViewModel = new LogbookViewModel() { Id = fakeId, Name = "logbook", LocationName = "location", AssignedManagers = fakeManagers, Categories = null };
            var controller = new LogbookController(logbookService.Object, mapper.Object, categoryService.Object, userService.Object);

            var sut = controller.UpdateManagers(fakeViewModel);

            logbookService.Verify(x => x.AssignManagers(fakeId, fakeManagers), Times.Once);
            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
        }
    }
}
