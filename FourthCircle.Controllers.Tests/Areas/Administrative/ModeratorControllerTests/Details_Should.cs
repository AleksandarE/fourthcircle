﻿using System;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Data.Entities;
using FourthCircle.Mappers;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.ModeratorControllerTests
{
    [TestClass]
    public class Details_Should
    {
        [TestMethod]
        public void Return_BadRequestResult_WhenIdIsNotValid()
        {
            var fakeId = "fakeId";
            Comment fakeComment = null;
            var commentService = new Mock<ICommentService>();
            var mapper = new CommentViewModelMapper();
            commentService.Setup(x => x.GetCommentById(fakeId)).Returns(fakeComment).Verifiable();

            var controller = new ModeratorController(commentService.Object,mapper);
            controller.ModelState.AddModelError("Text", "Required");

            var sut = controller.Details(fakeId);

            Assert.IsInstanceOfType(sut, typeof(BadRequestResult));
            commentService.Verify();
        }

        [TestMethod]
        public void Return_PartialViewModel_WhenIdIsValid()
        {
            var fakeId = "fakeId";
            Comment fakeComment = new Comment("author", "fake comment text", new Location("location"), true) { Id=fakeId};
            var commentService = new Mock<ICommentService>();
            var mapper = new CommentViewModelMapper();
            commentService.Setup(x => x.GetCommentById(fakeId)).Returns(fakeComment).Verifiable();

            var controller = new ModeratorController(commentService.Object, mapper);

            var sut = controller.Details(fakeId);

            Assert.IsInstanceOfType(sut, typeof(PartialViewResult));
            commentService.Verify();
        }
    }
}
