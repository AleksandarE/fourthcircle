﻿using System;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Data.Entities;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.ModeratorControllerTests
{
    [TestClass]
    public class DeleteComment_Should
    {
        [TestMethod]
        public void Return_BadRequestResult_WhenIdIsNotValid()
        {
            var fakeId = "fakeId";
            Comment fakeComment = null;
            var commentService = new Mock<ICommentService>();
            var mapper = new CommentViewModelMapper();
            commentService.Setup(x => x.GetCommentById(fakeId)).Returns(fakeComment).Verifiable();

            var controller = new ModeratorController(commentService.Object, mapper);
            controller.ModelState.AddModelError("Id", "Required");

            var sut = controller.DeleteComment(fakeId);

            Assert.IsInstanceOfType(sut, typeof(BadRequestResult));
            commentService.Verify();
        }

        [TestMethod]
        public void Redirect_WhenIdIsValid()
        {
            var fakeId = "fakeId";
            var fakeCommentText = "fake comment text";
            Comment fakeComment = new Comment("author", fakeCommentText, new Location("location"), true) { Id = fakeId };
            var commentService = new Mock<ICommentService>();
            var mapper = new CommentViewModelMapper();
            commentService.Setup(x => x.GetCommentById(fakeId)).Returns(fakeComment).Verifiable();
            var fakeViewModel = new CommentViewModel() { Id = fakeId, Author = "author", Text = fakeCommentText, LocationName = "location", IsHappy = true };
            var controller = new ModeratorController(commentService.Object, mapper);

            var sut = controller.DeleteComment(fakeId);

            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
            commentService.Verify(x => x.DeleteComment(fakeId), Times.Once);
        }
    }
}
