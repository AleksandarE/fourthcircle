﻿using System;
using System.Collections.Generic;
using System.Threading;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.UserControllerTests
{
    [TestClass]
    public class DeleteUser_Should
    {
        [TestMethod]
        public void Redirects_WhenIdIsNotValid()
        {
            var fakeId = "fakeId";
            var userService = new Mock<IUserService>();
            var mockContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockContext.Object);
            var mapper = new UserViewModelMapper(userManager);
            var roleManager = HelperTestMethods.GetRoleManager(mockContext.Object);

            var controller = new UserController(mapper, userService.Object,roleManager,userManager);
            controller.ModelState.AddModelError("UserName", "Required");
            var fakeUserViewModel = new UserViewModel() { Id=fakeId};
            var sut = controller.DeleteUser(fakeUserViewModel);

            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
        }

        [TestMethod]
        public void Redirect_WhenIdIsValid()
        {
            var fakeId = "fakeId";

            var userService = new Mock<IUserService>();
            userService.Setup(x => x.DeleteUser(It.IsAny<string>())).Verifiable();
            var mockContext = new Mock<ApplicationDbContext>();
            var mockUserStore = new Mock<IUserStore<ApplicationUser>>();
            var mockUserRoleStore = mockUserStore.As<IUserRoleStore<ApplicationUser>>();
            mockUserRoleStore.Setup(x => x.GetRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<CancellationToken>())).ReturnsAsync(It.IsAny<List<string>>);
            var userManager = new UserManager<ApplicationUser>(mockUserStore.Object, null, null, null, null, null, null, null, null);
            var mapper = new UserViewModelMapper(userManager);
            var roleManager = HelperTestMethods.GetRoleManager(mockContext.Object);

            var controller = new UserController(mapper, userService.Object,roleManager,userManager);

            var fakeUserViewModel = new UserViewModel() { Id = fakeId };
            var sut = controller.DeleteUser(fakeUserViewModel);

            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
            userService.Verify(x=>x.DeleteUser(It.IsAny<string>()),Times.Once);
        }
    }
}
