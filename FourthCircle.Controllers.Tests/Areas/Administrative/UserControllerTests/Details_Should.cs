﻿using System;
using System.Collections.Generic;
using System.Threading;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.UserControllerTests
{
    [TestClass]
    public class Details_Should
    {
        [TestMethod]
        public void Return_BadRequestResult_WhenIdIsNotValid()
        {
            var fakeId = "fakeId";
            var userService = new Mock<IUserService>();
            var mockContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockContext.Object);
            var mapper = new UserViewModelMapper(userManager);
            var roleManager = HelperTestMethods.GetRoleManager(mockContext.Object);

            var controller = new UserController(mapper, userService.Object,roleManager,userManager);
            controller.ModelState.AddModelError("UserName", "Required");

            var sut = controller.Details(fakeId);

            Assert.IsInstanceOfType(sut, typeof(BadRequestResult));

        }

        [TestMethod]
        public void Return_ViewModel_WhenIdIsValid()
        {
            var fakeId = "fakeId";
            var fakeUser = new ApplicationUser("fakeUser");
            var userService = new Mock<IUserService>();
            userService.Setup(x=>x.GetUserById(It.IsAny<string>())).Returns(fakeUser);
            var mockContext = new Mock<ApplicationDbContext>();
            var roleManager = HelperTestMethods.GetRoleManager(mockContext.Object);

            var mockUserStore = new Mock<IUserStore<ApplicationUser>>();
            var mockUserRoleStore = mockUserStore.As<IUserRoleStore<ApplicationUser>>();
            var userManager = new UserManager<ApplicationUser>(mockUserStore.Object, null, null, null, null, null, null, null, null);
            var fakeRoles = new List<string> { "role" };
            mockUserRoleStore.Setup(x => x.GetRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<CancellationToken>())).ReturnsAsync(new List<string> { "role" });

            var mapper = new UserViewModelMapper(userManager);

            var controller = new UserController(mapper, userService.Object,roleManager,userManager);

            var sut = controller.Details(fakeId);

            Assert.IsInstanceOfType(sut, typeof(PartialViewResult));
            userService.Verify();
        }
    }
}
