﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.UserControllerTests
{
    [TestClass]
    public class ListUsers_Should
    {
        [TestMethod]
        public async Task Return_ViewModel()
        {
            var userService = new Mock<IUserService>();
            var mockContext = new Mock<ApplicationDbContext>();
            var roleManager = HelperTestMethods.GetRoleManager(mockContext.Object);

            var mockUserStore = new Mock<IUserStore<ApplicationUser>>();
            var mockUserRoleStore = mockUserStore.As<IUserRoleStore<ApplicationUser>>();
            var userManager = new UserManager<ApplicationUser>(mockUserStore.Object, null, null, null, null, null, null, null, null);
            var fakeRoles = new List<string> { "role" };
            mockUserRoleStore.Setup(x => x.GetRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<CancellationToken>())).ReturnsAsync(new List<string> { "role"});

            var mapper = new UserViewModelMapper(userManager);
            var fakeUsers = new List<ApplicationUser> { new ApplicationUser("Joe") { Email = "fake email" }, new ApplicationUser("John") { Email = "fake email" } }.AsQueryable();
            userService.Setup(x => x.GetAllUsers()).Returns(fakeUsers);
            userService.Setup(x => x.FilterUsers(null, null)).Returns(fakeUsers).Verifiable();
            var controller = new UserController(mapper, userService.Object,roleManager,userManager);

            var sut = await controller.ListUsers(null, null, null, null);

            Assert.IsInstanceOfType(sut, typeof(ViewResult));
            userService.Verify(x => x.FilterUsers(null, null), Times.Once);
        }
    }
}
