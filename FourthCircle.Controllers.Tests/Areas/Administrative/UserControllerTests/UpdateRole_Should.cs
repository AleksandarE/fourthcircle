﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FourthCircle.Areas.Administrative.Controllers;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.UserControllerTests
{
    [TestClass]
    public class UpdateRole_Should
    {
        [TestMethod]
        public async Task Redirect_WhenIdIsNotValid()
        {
            var fakeId = "fakeId";
            var userService = new Mock<IUserService>();
            var mockContext = new Mock<ApplicationDbContext>();
            var userManager = HelperTestMethods.GetUserManager(mockContext.Object);
            var mapper = new UserViewModelMapper(userManager);
            var roleManager = HelperTestMethods.GetRoleManager(mockContext.Object);

            var controller = new UserController(mapper, userService.Object,roleManager,userManager);
            controller.ModelState.AddModelError("UserName", "Required");
            var fakeUserViewModel = new UserViewModel() { Id = fakeId };

            var sut = await controller.UpdateRole(fakeUserViewModel);

            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
        }

        [TestMethod]
        public async Task Redirect_WhenIdIsValid()
        {
            var fakeUser = new ApplicationUser("fakeUser") { Email = "test email" };
            var userService = new Mock<IUserService>();
            userService.Setup(x => x.ChangeRole(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(fakeUser).Verifiable();
            var mockContext = new Mock<ApplicationDbContext>();

            var mockUserStore = new Mock<IUserStore<ApplicationUser>>();
            var roleManager = HelperTestMethods.GetRoleManager(mockContext.Object);
            var userManager = new UserManager<ApplicationUser>(mockUserStore.Object, null, null, null, null, null, null, null, null);
            var mapper = new UserViewModelMapper(userManager);
            var controller = new UserController(mapper, userService.Object, roleManager, userManager);

            var fakeUserViewModel = new UserViewModel() { UserName = "test user", Email = "test email", UpdatableRole = "updated role" };
            var sut = await controller.UpdateRole(fakeUserViewModel);

            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
            userService.Verify(x => x.ChangeRole(It.IsAny<string>(), It.IsAny<string>()), Times.Once);

        }
    }
}
