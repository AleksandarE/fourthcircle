﻿using System;
using System.Collections.Generic;
using System.Linq;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FourthCircle.Areas.Administrative.Controllers;
using Moq;
using Microsoft.AspNetCore.Mvc;

namespace FourthCircle.Controllers.Tests.Areas.Administrative.HomeControllerTests
{
    [TestClass]
    public class Index_Should
    {
        [TestMethod]
        public void Return_ViewResult()
        {
            var mockUserService = new Mock<IUserService>();
            var fakeUsers = new List<ApplicationUser> { new ApplicationUser("Joe"), new ApplicationUser("John") }.AsQueryable();
            mockUserService.Setup(x=>x.GetAllUsers()).Returns(fakeUsers).Verifiable();
            var controller  = new FourthCircle.Areas.Administrative.Controllers.HomeController(mockUserService.Object);

            var sut = controller.Index();

            Assert.IsInstanceOfType(sut,typeof(ViewResult));
            mockUserService.Verify();
        }

        [TestMethod]
        public void RedirectToAction()
        {
            var mockUserService = new Mock<IUserService>();
            string fakeUrl = "fakeUrl";
            var controller = new FourthCircle.Areas.Administrative.Controllers.HomeController(mockUserService.Object);

            var sut = controller.Index(fakeUrl);

            Assert.IsInstanceOfType(sut, typeof(RedirectToActionResult));
        }
    }
}
