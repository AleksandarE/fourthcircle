# Circle  
### A Restaurant / Hotel managers logbook

| Members |
|:---------:|
|[Aleksandar Evangelatov](https://gitlab.com/AleksandarE/)|
|[Dragomir Dragnev](https://gitlab.com/dragnev)|

**Circle is the final project for Telerik Academy Alpha .NET 2019, developed for Fourth.**

The application allows Restaurant / Hotel managers to log notes during their daily activities and customers to post comments on the service. The application supports multiple differnet locations, with multiple users.

Customers are able to add comments with positive/negative service feedback and view others’ feedback. 
Managers can take notes (logs) about things happening on specific shift – for example unusual things, stuff related issues, TODOs, and sharing them between managers. These logs are organized into LogBooks, have categories and are logged for specific date and time.
User accounts are managed by administrators in the administrative area. Administrators initialize new LogBooks and Categories for them. They specify the access for each manager for each the LogBook. Administrator also manage the roles ot Moderators, responsible for editing customers' commnets.

### Public Section
The public is accessible without authentication. Users can easily leave a comment. Obscene comments are automatically filtered upon posting. The list of latest comments is also the landing page of the applicaiton. 

![img](src/Homepage.png)

![img](src/HomePage_addComment.png)

### Moderator Section
Moderators are granted access to view original versions of customer comments and have the ability to edit or delete customers' comments

![img](src/Moderator_view.png)

### Manager Section
Managers have access to their logbooks. Manager's notes are sotred per Logbook and managers are able to switch between logbooks and filter notes by date posted, text in note ot tag.

![img](src/Manager Notes_web.png)

![img](src/Manager_AddNote.png)

### Administrative Section
Administrators  create users and can change user roles. Administrators initialize new LogBooks and create Categories.  They specify the access for each manager for each the LogBook. 

![img](src/Administrative - Logbooks.png)

![img](src/Administrative-Users.png)

All of the above sections have responsive design and are easily accessible on moblie devices. 

* Homepage

![img](src/Homepage_Mobile_small.png)

* Manager Notes on phone sized screen

![img](src/Manager_Notes_mobile_small.png)

* Manager Notes on tablet sized screen

![img](src/Manager_Notes_mobile_Large.png)