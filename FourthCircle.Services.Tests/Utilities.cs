﻿using System;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FourthCircle.Services.Tests
{
    public static class Utilities
    {
        public static DbContextOptions<ApplicationDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<ApplicationDbContext>().UseInMemoryDatabase(databaseName).Options;
        }

        public static UserManager<ApplicationUser> GetUserManager(ApplicationDbContext context)
        {
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store, null, null, null, null, null, null, null, null);
            return userManager;
        }

        public static RoleManager<IdentityRole> GetRoleManager(ApplicationDbContext context)
        {
            var store = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(store, null, null, null, null);
            return roleManager;
        }
    }
}
