﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services.Tests.ManagerServiceTests
{
    [TestClass]
    public class EditNote_Should
    {
        [TestMethod]
        public void Succeed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WhenInputIsValid));

            var noteText = "noteTxt";
            var editedNoteText = "editedNoteTxt";
            var logbookName = "testLgbook";
            var username = "testUsr";
            string logbookId;
            string authorUserId;
            string noteId;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(logbookName)).Entity;
                var note = arrangeContext.Notes.Add(new Note(user, noteText, logbook)).Entity;
                authorUserId = user.Id;
                logbookId = logbook.Id;
                noteId = note.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.EditNote(authorUserId, noteId, logbookId, editedNoteText);

                Assert.IsTrue(sut.Text == editedNoteText);
                Assert.IsTrue(sut.UserId == authorUserId);
                Assert.IsTrue(sut.LastEditedOn != null);
            }
        }

        [TestMethod]
        public void Throw_IfNoteDoesntExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfNoteDoesntExist));

            var noteText = "noteTxt";
            var editedNoteText = "editedNoteTxt";
            string logbookId = null;
            string userId = null;
            string noteId = null;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                Assert.ThrowsException<ArgumentException>(() => managerService.EditNote(userId, noteText, logbookId, editedNoteText),
                    string.Format(Constants.NoteNotFoundException, noteId));
            }
        }

        [TestMethod]
        public void Throw_IfUserIdDoesntMatch()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfUserIdDoesntMatch));

            var noteText = "noteTxt";
            var editedNoteText = "editedNoteTxt";
            var logbookName = "testLgbook";
            var username = "testUsr";
            string logbookId;
            string unauthorizedUserId;
            string noteId;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var userAuthor = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var anotherUser = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(logbookName)).Entity;
                var note = arrangeContext.Notes.Add(new Note(userAuthor, noteText, logbook)).Entity;
                logbookId = logbook.Id;
                unauthorizedUserId = anotherUser.Id;
                noteId = note.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                Assert.ThrowsException<ArgumentException>(() => managerService.EditNote(unauthorizedUserId, noteText, logbookId, editedNoteText),
                    Constants.UserIDMissmatchException);
            }
        }

        [TestMethod]
        public void Throw_IfLogbookIdIsNull()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfLogbookIdIsNull));

            var noteText = "noteTxt";
            var editedNoteText = "editedNoteTxt";
            var logbookName = "testLgbook";
            var username = "testUsr";
            string logbookId = null;
            string userId;
            string noteId;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(logbookName)).Entity;
                var note = arrangeContext.Notes.Add(new Note(user, noteText, logbook)).Entity;
                userId = user.Id;
                noteId = note.Id;
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                Assert.ThrowsException<ArgumentException>(() => managerService.EditNote(userId, noteId, logbookId, editedNoteText),
                    string.Format(Constants.LogbookNotFoundException, logbookId));
            }
        }
    }
}
