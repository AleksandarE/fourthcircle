﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services.Tests.ManagerServiceTests
{
    [TestClass]
    public class FilterLogbookNotes_Should
    {
        //[TestMethod]
        public void OrderNotesCorrectly_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(OrderNotesCorrectly_WhenInputIsValid));

            var noteText = "noteTxt";
            var testLogbookName = "testLgbook";
            var username = "testUsr";
            var testNotesCount = 6;
            string logbookId;
            IEnumerable<Note> orderedNotes;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(testLogbookName)).Entity;

                for (int i = 0; i < testNotesCount; i++)
                {
                    arrangeContext.Notes.Add(new Note(user, noteText + i, logbook));
                }

                logbookId = logbook.Id;
                arrangeContext.SaveChanges();
                orderedNotes = arrangeContext.Notes.Where(n => n.LogbookId == logbook.Id)
                    .OrderByDescending(n => n.CreatedOn).ToList();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.FilterLogbookNotes(logbookId);

                Assert.IsTrue(sut.Count() == testNotesCount);
                Assert.AreEqual(orderedNotes, sut); //TODO: assert collections
            }
        }
    }
}
