﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services.Tests.ManagerServiceTests
{
    [TestClass]
    public class AddNote_Should
    {
        [TestMethod]
        public void Succeed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WhenInputIsValid));

            var noteText = "noteTxt";
            var logbookName = "testLgbook";
            var username = "testUsr";
            string logbookId;
            string userId;
            var inputCategories = new List<string> { "testCategry1", "testCategry2" };
            int addedNoteCount;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(logbookName)).Entity;
                userId = user.Id;
                logbookId = logbook.Id;
                foreach (var categoryName in inputCategories)
                {
                    var category = new Category(categoryName);
                    arrangeContext.LogbookCategories.Add(new LogbookCategories { Logbook = logbook, Category = category });
                }
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.AddNote(userId, noteText, logbookId, inputCategories);
                addedNoteCount = 1;

                Assert.IsTrue(assertContext.Notes.Count() == addedNoteCount);
                Assert.IsTrue(sut.Text == noteText);
                Assert.IsTrue(sut.UserId == userId);
                Assert.IsTrue(sut.LogbookId == logbookId);
                Assert.IsTrue(sut.NotesCategories.Count == inputCategories.Count);
            }
        }

        [TestMethod]
        public void Throw_IfUserIdIsNull()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfUserIdIsNull));

            var noteText = "noteTxt";
            var logbookName = "testLgbook";
            string logbookId;
            string userId = null;
            var inputCategories = new List<string> { "testCategry1", "testCategry2" };

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = arrangeContext.Logbooks.Add(new Logbook(logbookName)).Entity;
                logbookId = logbook.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                Assert.ThrowsException<ArgumentException>(() => managerService.AddNote(userId, noteText, logbookId, inputCategories),
                    string.Format(Constants.UserWithIdNotFoundException,userId));
            }
        }

        [TestMethod]
        public void Throw_IfLogbookIdIsNull()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfLogbookIdIsNull));

            var noteText = "noteTxt";
            string logbookId = null;
            string userId = null;
            var inputCategories = new List<string> { "testCategry1", "testCategry2" };

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                Assert.ThrowsException<ArgumentException>(() => managerService.AddNote(userId, noteText, logbookId, inputCategories),
                    string.Format(Constants.LogbookNotFoundException,logbookId));
            }
        }
    }
}
