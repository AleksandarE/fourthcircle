﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services.Tests.ManagerServiceTests
{
    [TestClass]
    public class GetNoteById_Should
    {
        [TestMethod]
        public void Succeed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WhenInputIsValid));

            var noteText = "noteTxt";
            var testLogbookName = "testLgbook";
            var username = "testUsr";
            string logbookId;
            string noteId;
            string userId;
            Note note;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(testLogbookName)).Entity;
                note = arrangeContext.Notes.Add(new Note(user, noteText, logbook)).Entity;
                userId = user.Id;
                noteId = note.Id;
                logbookId = logbook.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.GetNoteById(noteId);

                Assert.IsTrue(sut.Id == noteId);
                Assert.IsTrue(sut.UserId == userId);
                Assert.IsTrue(sut.LogbookId == note.LogbookId);
                Assert.IsTrue(sut.Text == note.Text);
            }
        }

        [TestMethod]
        public void Throw_IfNoteDoesntExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfNoteDoesntExist));

            var noteText = "noteTxt";
            var testLogbookName = "testLgbook";
            var username = "testUsr";
            string logbookId;
            string noteId = null;
            string userId;
            Note note;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(testLogbookName)).Entity;
                note = arrangeContext.Notes.Add(new Note(user, noteText, logbook)).Entity;
                userId = user.Id;
                logbookId = logbook.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                Assert.ThrowsException<ArgumentException>(() => managerService.GetNoteById(noteId),
                    string.Format(Constants.NoteNotFoundException, noteId));
            }
        }
    }
}
