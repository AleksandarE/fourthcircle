﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services.Tests.ManagerServiceTests
{
    [TestClass]
    public class DeleteNote_Should
    {
        [TestMethod]
        public void Succeed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WhenInputIsValid));

            var noteText = "noteTxt";
            var logbookName = "testLgbook";
            var username = "testUsr";
            string logbookId;
            string userId;
            string noteId;
            int testNoteCount;
            int deletedNoteCount;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(logbookName)).Entity;
                var note = arrangeContext.Notes.Add(new Note(user, noteText, logbook)).Entity;
                userId = user.Id;
                logbookId = logbook.Id;
                noteId = note.Id;
                arrangeContext.SaveChanges();
                testNoteCount = arrangeContext.Notes.Count();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.DeleteNote(userId, noteId, logbookId);
                deletedNoteCount = 1;

                Assert.IsTrue(assertContext.Notes.Count() == testNoteCount - deletedNoteCount);
                Assert.IsFalse(assertContext.Notes.Contains(sut));
            }
        }

        [TestMethod]
        public void Throw_IfNoteDoesntExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfNoteDoesntExist));

            var noteText = "noteTxt";
            string logbookId = null;
            string userId = null;
            string noteId = null;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                Assert.ThrowsException<ArgumentException>(() => managerService.DeleteNote(userId, noteText, logbookId),
                    string.Format(Constants.NoteNotFoundException,noteId));
            }
        }

        [TestMethod]
        public void Throw_IfUserIdDoesntMatch()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfUserIdDoesntMatch));

            var noteText = "noteTxt";
            var logbookName = "testLgbook";
            var username = "testUsr";
            string logbookId;
            string unauthorizedUserId;
            string noteId;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var anotherUser = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(logbookName)).Entity;
                var note = arrangeContext.Notes.Add(new Note(user, noteText, logbook)).Entity;
                logbookId = logbook.Id;
                unauthorizedUserId = anotherUser.Id;
                noteId = note.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                Assert.ThrowsException<ArgumentException>(() => managerService.DeleteNote(unauthorizedUserId, noteText, logbookId),
                    Constants.UserIDMissmatchException);
            }
        }
    }
}
