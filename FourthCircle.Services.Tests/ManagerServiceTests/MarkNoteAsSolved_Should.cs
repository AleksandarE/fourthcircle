﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services.Tests.ManagerServiceTests
{
    [TestClass]
    public class MarkNoteAsSolved_Should
    {
        [TestMethod]
        public void Succeed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WhenInputIsValid));

            var expectedToBeSolved = true;
            var noteText = "noteTxt";
            var logbookName = "testLgbook";
            var username = "testUsr";
            string logbookId;
            string userId;
            string noteId;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(logbookName)).Entity;
                var note = arrangeContext.Notes.Add(new Note(user, noteText, logbook)).Entity;
                userId = user.Id;
                logbookId = logbook.Id;
                noteId = note.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.MarkNoteAsSolved(userId, noteId, logbookId);

                Assert.IsTrue(sut.IsMarkedAsSolved == expectedToBeSolved);
            }
        }

        [TestMethod]
        public void Throw_IfNoteDoesntExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfNoteDoesntExist));

            string logbookId = null;
            string userId = null;
            string noteId = null;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                Assert.ThrowsException<ArgumentException>(() => managerService.MarkNoteAsSolved(userId, noteId, logbookId),
                    string.Format(Constants.NoteNotFoundException, noteId));
            }
        }

        [TestMethod]
        public void Throw_IfUserNotFound()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfUserNotFound));

            var noteText = "noteTxt";
            var logbookName = "testLgbook";
            var username = "testUsr";
            string logbookId;
            string userId = null;
            string noteId;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(logbookName)).Entity;
                var note = arrangeContext.Notes.Add(new Note(user, noteText, logbook)).Entity;
                logbookId = logbook.Id;
                noteId = note.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                Assert.ThrowsException<ArgumentException>(() => managerService.MarkNoteAsSolved(userId, noteId, logbookId),
                    string.Format(Constants.UserWithIdNotFoundException, userId));
            }
        }
    }
}
