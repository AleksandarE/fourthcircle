﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services.Tests.ManagerServiceTests
{
    [TestClass]
    public class GetLogbookCategories_Should
    {
        [TestMethod]
        public void ReturnCorrectEntityListCount_IfLogbookHasAnyCategories()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectEntityListCount_IfLogbookHasAnyCategories));
            var testLogbookName = "testLgbook";
            string logbookId;
            var inputCategories = new List<string> { "testCategry1", "testCategry2", "testCategry3" };
            var expectedcategoryList = new List<Category>();

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = arrangeContext.Logbooks.Add(new Logbook(testLogbookName)).Entity;
                logbookId = logbook.Id;
                foreach (var categoryName in inputCategories)
                {
                    var category = new Category(categoryName);
                    arrangeContext.LogbookCategories.Add(new LogbookCategories { Logbook = logbook, Category = category });
                }
                arrangeContext.SaveChanges();
                expectedcategoryList = arrangeContext.Categories.Where(c => c.LogbookCategories.Any(lc => lc.LogbookId == logbookId)).ToList();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.GetLogbookCategories(logbookId);

                Assert.IsTrue(sut.Count() == expectedcategoryList.Count);
            }
        }

        [TestMethod]
        public void ReturnEmptyList_IfLogbookHasNoCategories()
        {
            var options = Utilities.GetOptions(nameof(ReturnEmptyList_IfLogbookHasNoCategories));
            var logbookWithCategoriesName = "testLgbook";
            var username = "testUsr";
            string logbookId;
            string userId;
            var inputCategories = new List<string> { "testCategry1", "testCategry2", "testCategry3" };
            var expectedcategoryList = new List<Category>();
            int testCategoryCount = 0;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var testLogbook = arrangeContext.Logbooks.Add(new Logbook(logbookWithCategoriesName)).Entity;
                var logbookWithCategories = arrangeContext.Logbooks.Add(new Logbook(logbookWithCategoriesName)).Entity;
                logbookId = testLogbook.Id;
                foreach (var categoryName in inputCategories)
                {
                    var category = new Category(categoryName);
                    arrangeContext.LogbookCategories.Add(new LogbookCategories { Logbook = logbookWithCategories, Category = category });
                }
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.GetLogbookCategories(logbookId);

                Assert.IsTrue(sut.Count() == testCategoryCount);
            }
        }

        [TestMethod]
        public void ReturnEmptyList_IfLogbookDoesntExist()
        {
            var options = Utilities.GetOptions(nameof(ReturnEmptyList_IfLogbookDoesntExist));

            int testCategoryCount = 0;
            string logbookId = null;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.GetLogbookCategories(logbookId);

                Assert.IsTrue(sut.Count() == testCategoryCount);
            }
        }
    }
}
