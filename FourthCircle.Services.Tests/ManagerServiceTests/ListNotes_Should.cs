﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services.Tests.ManagerServiceTests
{
    [TestClass]
    public class ListNotes_Should
    {
        [TestMethod]
        public void ReturnCorrectEntityCount_IfLogbookCountainsNotes()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectEntityCount_IfLogbookCountainsNotes));

            var noteText = "noteTxt";
            var testLogbookName = "testLgbook";
            var username = "testUsr";
            var testNotesCount = 6;
            string logbookId;
            IEnumerable<Note> expectedNotesList;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var user = arrangeContext.Users.Add(new ApplicationUser(username)).Entity;
                var logbook = arrangeContext.Logbooks.Add(new Logbook(testLogbookName)).Entity;

                for (int i = 0; i < testNotesCount; i++)
                {
                    arrangeContext.Notes.Add(new Note(user, noteText + i, logbook));
                }

                logbookId = logbook.Id;
                arrangeContext.SaveChanges();
                expectedNotesList = arrangeContext.Notes
                    .Where(n => n.LogbookId == logbook.Id)
                    .ToList();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.ListNotes(logbookId);

                Assert.IsTrue(sut.Count() == expectedNotesList.Count());
            }
        }

        [TestMethod]
        public void ReturnEmptyList_IfLogbookHasNoNotes()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectEntityCount_IfLogbookCountainsNotes));

            var testLogbookName = "testLgbook";
            string logbookId;
            IEnumerable<Note> expectedNotesList;
            var testListEntitiesCount = 0;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = arrangeContext.Logbooks.Add(new Logbook(testLogbookName)).Entity;
                logbookId = logbook.Id;
                arrangeContext.SaveChanges();
                expectedNotesList = arrangeContext.Notes
                    .Where(n => n.LogbookId == logbook.Id)
                    .ToList();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userService = new UserService(assertContext, null, null);
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, userService);

                var sut = managerService.ListNotes(logbookId);

                Assert.IsTrue(sut.Count() == expectedNotesList.Count());
                Assert.IsTrue(sut.Count() == testListEntitiesCount);
            }
        }

        [TestMethod]
        public void Throw_IfLogbookDoesntExist()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectEntityCount_IfLogbookCountainsNotes));
            string logbookId = null;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var logbookService = new LogbookService(assertContext, null);
                var managerService = new ManagerService(assertContext, logbookService, null);

                Assert.ThrowsException<ArgumentException>(() => managerService.ListNotes(logbookId),
                    string.Format(Constants.LogbookNotFoundException, logbookId));
            }
        }
    }
}
