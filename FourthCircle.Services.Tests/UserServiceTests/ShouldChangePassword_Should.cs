﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.UserServiceTests
{
    [TestClass]
    public class ShouldChangePassword_Should
    {
        [TestMethod]
        public void Return_True_WhenPasswordMustBeChanged()
        {
            var options = Utilities.GetOptions(nameof(Return_True_WhenPasswordMustBeChanged));
            var username = "random user";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Users.Add(new ApplicationUser(username));
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userManager = Utilities.GetUserManager(assertContext);
                var roleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext,userManager, roleManager);

                var sut = userService.ShouldChangePassword(username);

                Assert.IsTrue(sut);
            }
        }

        [TestMethod]
        public void Return_False_WhenPasswordWasChanged()
        {
            var options = Utilities.GetOptions(nameof(Return_False_WhenPasswordWasChanged));
            var username = "random user";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Users.Add(new ApplicationUser(username));
                arrangeContext.SaveChanges();

                var targetUser = arrangeContext.Users.FirstOrDefault(x => x.UserName == username);
                targetUser.ChangePassword = false;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userManager = Utilities.GetUserManager(assertContext);
                var roleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext, userManager, roleManager);

                var sut = userService.ShouldChangePassword(username);

                Assert.IsFalse(sut);
            }
        }
    }
}
