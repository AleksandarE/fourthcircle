﻿using System;
using System.Collections.Generic;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FourthCircle.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetAvailableRoles_Should
    {
        [TestMethod]
        public void Return_Collection_WithCorrectNumberOfRoles()
        {
            var options = Utilities.GetOptions(nameof(Return_Collection_WithCorrectNumberOfRoles));
            var username = "user";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var fakeUser = new ApplicationUser(username);

                var arrangeRoleManager = Utilities.GetRoleManager(arrangeContext);
                arrangeRoleManager.CreateAsync(new IdentityRole("ArrangeRole"));
                arrangeRoleManager.CreateAsync(new IdentityRole("AssertRole"));
                arrangeRoleManager.CreateAsync(new IdentityRole("TestRole"));

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var assertUserManager = Utilities.GetUserManager(assertContext);
                var assertRoleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext, assertUserManager, assertRoleManager);

                var sut = userService.GetAvailableRoles();

                Assert.IsInstanceOfType(sut, typeof(List<SelectListItem>));
                Assert.IsTrue(sut.Count == 3);
                Assert.IsTrue(sut.Any(x=>x.Value == "TestRole"));
            }
        }
    }
}
