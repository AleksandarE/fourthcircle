﻿using System;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.UserServiceTests
{
    [TestClass]
    public class SuccessfulPasswordChange_Should
    {
        [TestMethod]
        public void Return_True_OnSuccessfulPasswordChange()
        {
            var options = Utilities.GetOptions(nameof(Return_True_OnSuccessfulPasswordChange));
            var username = "user";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Users.Add(new ApplicationUser(username));
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userManager = Utilities.GetUserManager(assertContext);
                var roleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext, userManager, roleManager);

                var targetUser = userService.GetUserByName(username);
                var sut = userService.SuccessfulPasswordChange(targetUser);

                Assert.IsTrue(targetUser.ChangePassword == false);
                Assert.IsTrue(sut);
            }
        }
    }
}
