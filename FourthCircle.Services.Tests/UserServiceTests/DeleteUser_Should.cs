﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.UserServiceTests
{
    [TestClass]
    public class DeleteUser_Should
    {
        [TestMethod]
        public void Succeed_WithValidInput()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WithValidInput));
            var user1 = "user1";
            var user2 = "user2";
            var user3 = "user3";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Users.Add(new ApplicationUser(user1));
                arrangeContext.Users.Add(new ApplicationUser(user2));
                arrangeContext.Users.Add(new ApplicationUser(user3));
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userManager = Utilities.GetUserManager(assertContext);
                var roleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext, userManager, roleManager);

                var targetUser = assertContext.Users.FirstOrDefault(x=>x.UserName == user2);

                userService.DeleteUser(targetUser.Id);

                Assert.IsTrue(assertContext.Users.Count() == 2);
                Assert.IsFalse(assertContext.Users.Any(x=>x.UserName == user2));
            }
        }
    }
}
