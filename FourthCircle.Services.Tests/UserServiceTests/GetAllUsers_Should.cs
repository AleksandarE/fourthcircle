﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetAllUsers_Should
    {
        [TestMethod]
        public void Return_IQueryable_AllUsers()
        {
            var options = Utilities.GetOptions(nameof(Return_IQueryable_AllUsers));
            var userCount = 10;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                for (int i = 1; i <= userCount; i++)
                {
                    arrangeContext.Users.Add(new ApplicationUser("user"+i));
                }
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userManager = Utilities.GetUserManager(assertContext);
                var roleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext, userManager,roleManager);

                var sut = userService.GetAllUsers();

                Assert.IsInstanceOfType(sut, typeof(IQueryable<ApplicationUser>));
                Assert.IsTrue(assertContext.Users.Count() == userCount);
                Assert.IsTrue(assertContext.Users.Any(x => x.UserName == "user10"));
            }
        }
    }
}
