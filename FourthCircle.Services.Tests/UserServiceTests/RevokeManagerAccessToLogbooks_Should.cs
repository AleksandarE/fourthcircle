﻿using System;
using System.Collections.Generic;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.UserServiceTests
{
    [TestClass]
    public class RevokeManagerAccessToLogbooks_Should
    {
        [TestMethod]
        public void Return_Instance_ApplicationUserWithNoLogbooks()
        {
            var options = Utilities.GetOptions(nameof(Return_Instance_ApplicationUserWithNoLogbooks));
            var username = "random user";
            var logbook = "test logbook";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Users.Add(new ApplicationUser(username));
                arrangeContext.Logbooks.Add(new Logbook(logbook));
                arrangeContext.SaveChanges();
                var fakeUser = arrangeContext.Users.FirstOrDefault(x=>x.UserName == username);
                var fakeLogbook = arrangeContext.Logbooks.FirstOrDefault(x=>x.Name == logbook);
                arrangeContext.LogbooksUsers.Add(new LogbooksUsers { User = fakeUser,Logbook = fakeLogbook});
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userManager = Utilities.GetUserManager(assertContext);
                var roleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext, userManager, roleManager);
                var targetUser = userService.GetUserByName(username);

                var sut = userService.RevokeManagerAccessToLogbooks(targetUser);

                Assert.IsInstanceOfType(sut,typeof(ApplicationUser));
                Assert.IsTrue(sut.LogbooksUsers.Count == 0);
            }
        }
    }
}
