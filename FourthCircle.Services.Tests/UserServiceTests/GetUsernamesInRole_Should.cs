﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetUsernamesInRole_Should
    {
        [TestMethod]
        public async Task Return_Collection_WithCorrectNumberOfUsers()
        {
            var options = Utilities.GetOptions(nameof(Return_Collection_WithCorrectNumberOfUsers));
            var username = "user";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var fakeUser = new ApplicationUser(username);

                var arrangeRoleManager = Utilities.GetRoleManager(arrangeContext);
                await arrangeRoleManager.CreateAsync(new IdentityRole("ArrangeRole"));
                await arrangeRoleManager.CreateAsync(new IdentityRole("AssertRole"));

                var arrangeUserManager = Utilities.GetUserManager(arrangeContext);
                for (int i = 1; i <= 5; i++)
                {
                    await arrangeUserManager.CreateAsync(new ApplicationUser(username + $"{i}"));
                    var targetUser = arrangeUserManager.Users.FirstOrDefault(x=>x.UserName == username + $"{i}");
                    await arrangeUserManager.AddToRoleAsync(targetUser, "ArrangeRole");
                }

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var assertUserManager = Utilities.GetUserManager(assertContext);
                var assertRoleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext, assertUserManager, assertRoleManager);

                var sut = await userService.GetUsernamesInRole("ArrangeRole");

                Assert.IsInstanceOfType(sut, typeof(ICollection<string>));
                Assert.IsTrue(sut.Count == 5);
                Assert.IsTrue(sut.Contains("user5"));
            }
        }
    }
}
