﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetUserById_Should
    {
        [TestMethod]
        public void Return_Instance_WhenUserExists()
        {
            var options = Utilities.GetOptions(nameof(Return_Instance_WhenUserExists));
            var username = "Pesho";
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Users.Add(new ApplicationUser(username));
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userManager = Utilities.GetUserManager(assertContext);
                var roleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext, userManager, roleManager);

                var targetUser = assertContext.Users.FirstOrDefault(x=>x.UserName == username);
                var sut = userService.GetUserById(targetUser.Id);
                
                Assert.IsInstanceOfType(sut, typeof(ApplicationUser));
                Assert.IsTrue(sut.UserName == username);
            }
        }
        [TestMethod]
        public void Throw_WhenUserDoesNotExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_WhenUserDoesNotExist));
            var Id = "random Id";

            using (var context = new ApplicationDbContext(options))
            {
                var userManager = Utilities.GetUserManager(context);
                var roleManager = Utilities.GetRoleManager(context);
                var userService = new UserService(context, userManager, roleManager);

                Assert.ThrowsException<ArgumentException>(()=> userService.GetUserById(Id), string.Format(Constants.UserWithIdNotFoundException, Id));
            }
        }
    }
}
