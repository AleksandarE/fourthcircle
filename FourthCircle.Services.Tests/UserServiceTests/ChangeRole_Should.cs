﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.UserServiceTests
{
    [TestClass]
    public class ChangeRole_Should
    {
        [TestMethod]
        public async Task Return_Instance_WithUpdatedRole()
        {
            var options = Utilities.GetOptions(nameof(Return_Instance_WithUpdatedRole));
            var username = "user";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var fakeUser = new ApplicationUser(username);

                var arrangeRoleManager = Utilities.GetRoleManager(arrangeContext);
                await arrangeRoleManager.CreateAsync(new IdentityRole("ArrangeRole"));
                await arrangeRoleManager.CreateAsync(new IdentityRole("AssertRole"));

                var arrangeUserManager = Utilities.GetUserManager(arrangeContext);
                await arrangeUserManager.CreateAsync(fakeUser);
                arrangeContext.SaveChanges();

                var roles = await arrangeUserManager.GetRolesAsync(fakeUser);
                foreach (var item in roles)
                {
                   await arrangeUserManager.RemoveFromRoleAsync(fakeUser,item);
                }

                await arrangeUserManager.AddToRoleAsync(fakeUser, "ArrangeRole");

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var assertUserManager = Utilities.GetUserManager(assertContext);
                var assertRoleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext, assertUserManager, assertRoleManager);

                var targetUser = userService.GetUserByName(username);
                var sut = await userService.ChangeRole(targetUser.Id,"AssertRole");

                Assert.IsInstanceOfType(sut,typeof(ApplicationUser));
                Assert.IsTrue(await assertUserManager.IsInRoleAsync(targetUser,"AssertRole"));
            }
        }
    }
}
