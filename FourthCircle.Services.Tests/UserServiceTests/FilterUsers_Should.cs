﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.UserServiceTests
{
    [TestClass]
    public class FilterUsers_Should
    {

        [DataRow("name_desc")]
        [TestMethod]
        public void Return_IOrderedEnumerable(string sortOrder)
        {
            var options = Utilities.GetOptions(nameof(Return_IOrderedEnumerable));
            var username = "User";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var arrangeUserManager = Utilities.GetUserManager(arrangeContext);
                for (int i = 1; i <= 5; i++)
                {
                    arrangeUserManager.CreateAsync(new ApplicationUser(username + $"{i}"));
                    var targetUser = arrangeUserManager.Users.FirstOrDefault(x => x.UserName == username + $"{i}");
                }

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userManager = Utilities.GetUserManager(assertContext);
                var roleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext,userManager,roleManager);

                var sut = userService.FilterUsers(sortOrder, null);

                Assert.IsInstanceOfType(sut, typeof(IOrderedQueryable<ApplicationUser>));
            }
        }

        [TestMethod]
        public void Return_Collection_SearchResults()
        {
            var options = Utilities.GetOptions(nameof(Return_Collection_SearchResults));
            var username = "User";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var arrangeUserManager = Utilities.GetUserManager(arrangeContext);
                for (int i = 1; i <= 5; i++)
                {
                    arrangeUserManager.CreateAsync(new ApplicationUser(username + $"{i}"));
                }
                arrangeUserManager.CreateAsync(new ApplicationUser("Pesho") { Email = "test@email.com" });
                arrangeUserManager.CreateAsync(new ApplicationUser("Gosho") { Email = "test@email.com" });
                arrangeUserManager.CreateAsync(new ApplicationUser("Tosho") { Email = "User6@email.com"});

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var userManager = Utilities.GetUserManager(assertContext);
                var roleManager = Utilities.GetRoleManager(assertContext);
                var userService = new UserService(assertContext, userManager, roleManager);

                var sut = userService.FilterUsers(null,username);

                Assert.IsTrue(sut.Count() == 6);
                Assert.IsTrue(sut.Any(x => x.UserName == "Tosho"));
                Assert.IsFalse(sut.Any(x=>x.UserName == "Pesho"));
                Assert.IsFalse(sut.Any(x => x.UserName == "Gosho"));
            }
        }
    }
}
