﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace FourthCircle.Services.Tests.CategotyServiceTests
{
    [TestClass]
    public class CreateCategory_Should
    {
        [TestMethod]
        public void Succed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succed_WhenInputIsValid));
            string categoryName = "categoryName";
            var testCategoriesCount = 1;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var categoryService = new CategoryService(assertContext);

                var sut = categoryService.CreateCategory(categoryName);

                Assert.IsInstanceOfType(sut, typeof(Category));
                Assert.IsTrue(sut.Name == categoryName);
                Assert.IsTrue(assertContext.Categories.Count() == testCategoriesCount);
            }
        }

        [TestMethod]
        public void Throw_IfNameIsNull()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfNameIsNull));
            string categoryName = null;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var categoryService = new CategoryService(assertContext);

                Assert.ThrowsException<ArgumentException>(() => categoryService.CreateCategory(categoryName),
                    Constants.CategoryNameMissingException);
            }
        }
    }
}
