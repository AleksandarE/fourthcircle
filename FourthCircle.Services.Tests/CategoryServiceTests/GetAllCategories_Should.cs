﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace FourthCircle.Services.Tests.CategotyServiceTests
{
    [TestClass]
    public class GetAllCategories_Should
    {
        [TestMethod]
        public void Return_CorrectCountOfEntities()
        {
            var options = Utilities.GetOptions(nameof(Return_CorrectCountOfEntities));
            var testCategoriesCount = 6;
            var testCategoryName = "categoryName";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                for (int i = 1; i <= testCategoriesCount; i++)
                {
                    arrangeContext.Categories.Add(new Category(testCategoryName+i));
                }
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var categoryService = new CategoryService(assertContext);

                var sut = categoryService.GetAllCategories();

                Assert.IsTrue(sut.Count() == testCategoriesCount);
            }
        }

        [TestMethod]
        public void ReturnEmptyList_IfNoCategoriesExist()
        {
            var options = Utilities.GetOptions(nameof(ReturnEmptyList_IfNoCategoriesExist));
            var testCategoriesCount = 0;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var categoryService = new CategoryService(assertContext);

                var sut = categoryService.GetAllCategories();

                Assert.IsTrue(sut.Count() == testCategoriesCount);
            }
        }
    }
}
