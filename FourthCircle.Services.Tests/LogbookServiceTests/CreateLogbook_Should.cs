﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace FourthCircle.Services.Tests.LogbookServiceTests
{
    [TestClass]
    public class CreateLogbook_Should
    {
        [TestMethod]
        public void Throw_WhenLobgookExistsAtLocation()
        {
            var options = Utilities.GetOptions(nameof(Throw_WhenLobgookExistsAtLocation));

            string locationName = "testLocation";
            string logbookName = "testLogbook";

            using(var arrangeContext = new ApplicationDbContext(options))
            {
                var testLocation = new Location(locationName);
                var testLogbook = new Logbook(logbookName);
                testLocation.Logbooks.Add(testLogbook);
                arrangeContext.Locations.Add(testLocation);
                arrangeContext.SaveChanges();
            }

            using(var assertContext = new ApplicationDbContext(options))
            {
                var testCategories = new string[] { "category1","category2"};
                var testManagers = new string[] { "manager1", "manager2" };
                var locationService = new LocationService(assertContext);
                var testLogbookService = new LogbookService(assertContext,locationService);

                Assert.ThrowsException<ArgumentException>(()=>testLogbookService.CreateLogbook(logbookName,locationName,testCategories,testManagers),
                    string.Format(Constants.LocationContainsLogbookWithSameNameException,locationName,logbookName));
            }
        }

        [TestMethod]
        public void Succed_WithValidInput()
        {
            var options = Utilities.GetOptions(nameof(Succed_WithValidInput));

            string locationName = "testLocation";
            string logbookName = "testLogbook";
            var testCategories = new string[] { "category1", "category2" };
            var testManagers = new string[] { "manager1", "manager2" };

            using(var context = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(context);
                var logbookService = new LogbookService(context,locationService);

                var sut = logbookService.CreateLogbook(logbookName,locationName,testCategories,testManagers);

                Assert.IsInstanceOfType(sut,typeof(Logbook));
                Assert.IsTrue(sut.Name == logbookName);
                Assert.IsTrue(sut.Location.Name == locationName);
                Assert.IsTrue(sut.LogbooksUsers.Count == testManagers.Count());
                Assert.IsTrue(sut.LogbookCategories.Count == testCategories.Count());
            }
        }

    }
}
