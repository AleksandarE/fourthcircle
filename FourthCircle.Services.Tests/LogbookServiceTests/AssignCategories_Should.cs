﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.LogbookServiceTests
{
    [TestClass]
    public class AssignCategories_Should
    {
        [TestMethod]
        public void Throw_WhenLogbookDoesNotExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_WhenLogbookDoesNotExist));
            string testId = "testId";
            var testCategories = new string[] { "category1", "category2" };

            using (var context = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(context);
                var logbookService = new LogbookService(context, locationService);

                Assert.ThrowsException<ArgumentException>(()=>logbookService.AssignCategories(testId,testCategories), 
                        string.Format(Constants.LogbookNotFoundException,testId));
            }
        }

        [TestMethod]
        public void Succeeds_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeeds_WhenInputIsValid));
            var logbookName = "testName";
            var testCategories = new string[] { "random", "second random category" };

            using(var arrangeContext = new ApplicationDbContext(options))
            {
                foreach (var item in testCategories)
                {
                    arrangeContext.Categories.Add(new Category(item));
                }
                arrangeContext.Logbooks.Add(new Logbook(logbookName));
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var logbookService = new LogbookService(assertContext, locationService);

                var sut = logbookService.AssignCategories(assertContext.Logbooks
                        .FirstOrDefault(x=>x.Name == logbookName).Id, testCategories);

                Assert.IsNotNull(sut.LogbookCategories);
                Assert.IsTrue(sut.LogbookCategories.Select(x => x.Category).Count() == testCategories.Count());
                Assert.IsTrue(sut.LogbookCategories.Select(x=>x.Category).Any(x=>x.Name == testCategories[1]));
            }
        }
    }
}
