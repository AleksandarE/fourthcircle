﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.LogbookServiceTests
{
    [TestClass]
    public class GetLogbooksByUser_Should
    {
        [TestMethod]
        public void Return_InstanceOfIQueryable()
        {
            var options = Utilities.GetOptions(nameof(Return_InstanceOfIQueryable));
            var username = "Pesho";
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Users.Add(new ApplicationUser(username));
                arrangeContext.SaveChanges();
            }

            using (var context = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(context);
                var logbookService = new LogbookService(context, locationService);
                var targetUser = context.Users.FirstOrDefault(x => x.UserName == username);

                var sut = logbookService.GetLogbooksByUser(targetUser.Id);

                Assert.IsInstanceOfType(sut, typeof(IQueryable<Logbook>));
            }
        }

        [TestMethod]
        public void Return_CorrectCountOfLogbooks()
        {
            var options = Utilities.GetOptions(nameof(Return_CorrectCountOfLogbooks));
            var username = "Pesho"; 

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Users.Add(new ApplicationUser(username));
                arrangeContext.Logbooks.Add(new Logbook("test"));
                arrangeContext.Logbooks.Add(new Logbook("test2"));
                arrangeContext.SaveChanges();
                var targetUser = arrangeContext.Users.FirstOrDefault(x => x.UserName == username);
                var fakeLogbook = arrangeContext.Logbooks.FirstOrDefault(x => x.Name == "test");
                var fakeLogbook2 = arrangeContext.Logbooks.FirstOrDefault(x => x.Name == "test2");
                fakeLogbook.LogbooksUsers.Add(new LogbooksUsers { User = targetUser, Logbook = fakeLogbook });
                fakeLogbook2.LogbooksUsers.Add(new LogbooksUsers { User = targetUser, Logbook = fakeLogbook2 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var logbookService = new LogbookService(assertContext, locationService);

                var sut = logbookService.FilterLogbooks(null,null,username);

                Assert.IsTrue(sut.Count() == 2);
            }
        }
    }
}
