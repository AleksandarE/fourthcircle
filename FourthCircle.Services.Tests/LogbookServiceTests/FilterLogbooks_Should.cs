﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.LogbookServiceTests
{
    [TestClass]
    public class FilterLogbooks_Should
    {
        [TestMethod]
        public void Return_IQueryable()
        {
            var options = Utilities.GetOptions(nameof(Return_IQueryable));

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var logbookService = new LogbookService(assertContext, locationService);

                var sut = logbookService.FilterLogbooks(null, null, null);

                Assert.IsInstanceOfType(sut, typeof(IQueryable<Logbook>));
            }
        }

        [TestMethod]
        public void Return_CorrectCountOfLogbooks_OnLocationSearch()
        {
            var options = Utilities.GetOptions(nameof(Return_CorrectCountOfLogbooks_OnLocationSearch));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Logbooks.Add(new Logbook("test") { Location = new Location("Restaurant")});
                arrangeContext.Logbooks.Add(new Logbook("test2") { Location = new Location("Restaurant") });
                arrangeContext.Logbooks.Add(new Logbook("test3") { Location = new Location("Bar") });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var logbookService = new LogbookService(assertContext, locationService);

                var sut = logbookService.FilterLogbooks(null,"Restaurant",null);

                Assert.IsTrue(sut.Count() == 2);
            }
        }

        [TestMethod]
        public void Return_CorrectCountOfLogbooks_OnUserSearch()
        {
            var options = Utilities.GetOptions(nameof(Return_CorrectCountOfLogbooks_OnUserSearch));
            var username = "Pesho";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var targetUser = arrangeContext.Users.FirstOrDefault(x => x.UserName == username);
                arrangeContext.Logbooks.Add(new Logbook("test") { Location = new Location("Restaurant") });
                arrangeContext.Logbooks.Add(new Logbook("test2") { Location = new Location("Restaurant") });
                arrangeContext.Logbooks.Add(new Logbook("test3") { Location = new Location("Bar") });
                arrangeContext.SaveChanges();
                var fakeLogbook = arrangeContext.Logbooks.FirstOrDefault(x => x.Name == "test");
                var fakeLogbook2 = arrangeContext.Logbooks.FirstOrDefault(x => x.Name == "test2");
                fakeLogbook.LogbooksUsers.Add(new LogbooksUsers { User = targetUser, Logbook = fakeLogbook });
                fakeLogbook.LogbooksUsers.Add(new LogbooksUsers { User = targetUser, Logbook = fakeLogbook2 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var logbookService = new LogbookService(assertContext, locationService);

                var sut = logbookService.FilterLogbooks(null, "Restaurant", null);

                Assert.IsTrue(sut.Count() == 2);
            }
        }

        [DataRow("location_desc")]
        [DataRow("logbook_desc")]
        [DataRow("logbook_name")]
        [TestMethod]
        public void Return_IOrderedEnumerable(string sortOrder)
        {
            var options = Utilities.GetOptions(nameof(Return_IOrderedEnumerable));
            var username = "Pesho";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var targetUser = arrangeContext.Users.FirstOrDefault(x => x.UserName == username);
                arrangeContext.Logbooks.Add(new Logbook("test") { Location = new Location("Restaurant") });
                arrangeContext.Logbooks.Add(new Logbook("test2") { Location = new Location("Restaurant") });
                arrangeContext.Logbooks.Add(new Logbook("test3") { Location = new Location("Bar") });
                arrangeContext.SaveChanges();
                var fakeLogbook = arrangeContext.Logbooks.FirstOrDefault(x => x.Name == "test");
                var fakeLogbook2 = arrangeContext.Logbooks.FirstOrDefault(x => x.Name == "test2");
                fakeLogbook.LogbooksUsers.Add(new LogbooksUsers { User = targetUser, Logbook = fakeLogbook });
                fakeLogbook.LogbooksUsers.Add(new LogbooksUsers { User = targetUser, Logbook = fakeLogbook2 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var logbookService = new LogbookService(assertContext, locationService);

                var sut = logbookService.FilterLogbooks(sortOrder, null, null);

                Assert.IsInstanceOfType(sut, typeof(IOrderedQueryable<Logbook>));
            }
        }
    }
}
