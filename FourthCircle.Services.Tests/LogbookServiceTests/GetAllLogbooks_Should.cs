﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.LogbookServiceTests
{
    [TestClass]
    public class GetAllLogbooks_Should
    {
        [TestMethod]
        public void Return_InstanceOfIQueryable()
        {
            var options = Utilities.GetOptions(nameof(Return_InstanceOfIQueryable));

            using (var context = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(context);
                var logbookService = new LogbookService(context,locationService);

                var sut = logbookService.GetAllLogbooks();

                Assert.IsInstanceOfType(sut,typeof(IQueryable<Logbook>));
            }
        }

        [TestMethod]
        public void Return_CorrectCountOfLogbooks()
        {
            var options = Utilities.GetOptions(nameof(Return_CorrectCountOfLogbooks));
            var logbooksCount = 5;

            using(var arrangeContext = new ApplicationDbContext(options))
            {
                for (int i = 1; i <= logbooksCount; i++)
                {
                    arrangeContext.Logbooks.Add(new Logbook($"test{i}"));
                }
                arrangeContext.SaveChanges();
            }

            using(var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var logbookService = new LogbookService(assertContext, locationService);

                var sut = logbookService.GetAllLogbooks();

                Assert.IsTrue(sut.Count() == logbooksCount);
            }
        }
    }
}
