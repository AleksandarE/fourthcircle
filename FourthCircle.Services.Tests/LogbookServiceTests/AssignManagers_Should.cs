﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.LogbookServiceTests
{
    [TestClass]
    public class AssignManagers_Should
    {
        [TestMethod]
        public void Throw_WhenLogbookDoesNotExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_WhenLogbookDoesNotExist));
            string testId = "testId";
            var testCategories = new string[] { "category1", "category2" };

            using (var context = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(context);
                var logbookService = new LogbookService(context, locationService);

                Assert.ThrowsException<ArgumentException>(() => logbookService.AssignCategories(testId, testCategories),
                        string.Format(Constants.LogbookNotFoundException,testId));
            }
        }

        [TestMethod]
        public void Succeeds_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeeds_WhenInputIsValid));
            var logbookName = "testName";
            var testManagers = new string[] { "manager", "cooler manager" };

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                foreach (var item in testManagers)
                {
                    arrangeContext.Users.Add(new ApplicationUser(item));
                }
                arrangeContext.Logbooks.Add(new Logbook(logbookName));
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var logbookService = new LogbookService(assertContext, locationService);

                var sut = logbookService.AssignManagers(assertContext.Logbooks
                        .FirstOrDefault(x => x.Name == logbookName).Id, testManagers);

                Assert.IsNotNull(sut.LogbooksUsers);
                Assert.IsTrue(sut.LogbooksUsers.Select(x => x.User).Count() == testManagers.Count());
                Assert.IsTrue(sut.LogbooksUsers.Select(x => x.User).Any(x => x.UserName == testManagers[1]));
            }
        }
    }
}
