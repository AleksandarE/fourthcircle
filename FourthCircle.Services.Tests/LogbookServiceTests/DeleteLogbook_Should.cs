﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.LogbookServiceTests
{
    [TestClass]
    public class DeleteLogbook_Should
    {
        [TestMethod]
        public void Succed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succed_WhenInputIsValid));
            var logbooksCount = 10;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                for (int i = 1; i <= logbooksCount; i++)
                {
                    arrangeContext.Logbooks.Add(new Logbook($"test{i}"));
                }
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var logbookService = new LogbookService(assertContext, locationService);

                var targetLogbook = assertContext.Logbooks.FirstOrDefault(x=>x.Name == "test8");

                logbookService.DeleteLogbook(targetLogbook);

                Assert.IsTrue(assertContext.Logbooks.Count() == logbooksCount-1);
                Assert.IsFalse(assertContext.Logbooks.Contains(targetLogbook));
            }
        }
    }
}
