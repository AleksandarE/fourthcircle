﻿using System;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.LogbookServiceTests
{
    [TestClass]
    public class GetLogbook_Should
    {
        [TestMethod]
        public void Return_Instance_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Return_Instance_WhenInputIsValid));
            string Id = "";
            string logbookName = "test";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Logbooks.Add(new Logbook(logbookName));
                arrangeContext.SaveChanges();
                Id = arrangeContext.Logbooks.FirstOrDefault(x=>x.Name == logbookName).Id;
            }

            using(var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var logbookService = new LogbookService(assertContext, locationService);

                var sut = logbookService.GetLogbook(Id);

                Assert.IsInstanceOfType(sut,typeof(Logbook));
                Assert.IsTrue(sut.Id == Id);
                Assert.IsTrue(sut.Name == logbookName);
            }
        }

        [TestMethod]
        public void Return_Null_LogbookDoesNotExist()
        {
            var options = Utilities.GetOptions(nameof(Return_Instance_WhenInputIsValid));
            string Id = "someId";

            using (var context = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(context);
                var logbookService = new LogbookService(context, locationService);

                var sut = logbookService.GetLogbook(Id);

                Assert.IsNull(sut);
            }
        }
    }
}
