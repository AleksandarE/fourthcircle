﻿using System;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.LocationServiceTests
{
    [TestClass]
    public class CreateLocation_Should
    {
        [TestMethod]
        public void Throw_WhenLocationExists()
        {
            var options = Utilities.GetOptions(nameof(Throw_WhenLocationExists));
            string locationName = "Location";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Locations.Add(new Location(locationName));
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);

                Assert.ThrowsException<ArgumentException>(() => locationService.CreateLoctation(locationName), 
                    string.Format(Constants.LocationAlreadyExistsException,locationName));
            }
        }

        [TestMethod]
        public void Succed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succed_WhenInputIsValid));
            string locationName = "Location";

            using (var context = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(context);

                var sut = locationService.CreateLoctation(locationName);

                Assert.IsInstanceOfType(sut, typeof(Location));
                Assert.IsTrue(sut.Name == locationName);
            }
        }
    }
}
