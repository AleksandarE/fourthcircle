﻿using System;
using System.Collections.Generic;
using System.Linq;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.LocationServiceTests
{
    [TestClass]
    public class GetLocationNames_Should
    {
        [TestMethod]
        public void Return_AllLocationNames()
        {
            var options = Utilities.GetOptions(nameof(Return_AllLocationNames));
            int locationCount = 10;
            string locationName = "Location";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                for (int i = 1; i <= locationCount; i++)
                {
                    arrangeContext.Locations.Add(new Location(locationName + i));
                }
                arrangeContext.SaveChanges();
            }

            using (var context = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(context);

                var sut = locationService.GetLocationNames();

                Assert.IsInstanceOfType(sut, typeof(ICollection<string>));
                Assert.IsTrue(context.Locations.Count() == locationCount);
                Assert.IsTrue(context.Locations.Any(x => x.Name == "Location3"));
            }
        }
    }
}
