﻿using System;
using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FourthCircle.Services.Tests.LocationServiceTests
{
    [TestClass]
    public class GetLocation_Should
    {
        [TestMethod]
        public void Return_Instance_WhenLocationExists()
        {
            var options = Utilities.GetOptions(nameof(Return_Instance_WhenLocationExists));
            string locationName = "Test Location";

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Locations.Add(new Location(locationName));
                arrangeContext.SaveChanges();
            }

            using (var context = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(context);

                var sut = locationService.GetLocation(locationName);

                Assert.IsInstanceOfType(sut, typeof(Location));
                Assert.IsTrue(sut.Name == locationName);
            }
        }

        [TestMethod]
        public void Return_Null_WhenLocationDoesNotExist()
        {
            var options = Utilities.GetOptions(nameof(Return_Null_WhenLocationDoesNotExist));
            string locationName = "Test Location";

            using (var context = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(context);

                var sut = locationService.GetLocation(locationName);

                Assert.IsNull(sut);
            }
        }
    }
}
