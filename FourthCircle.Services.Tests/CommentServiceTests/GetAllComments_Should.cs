﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System;

namespace FourthCircle.Services.Tests.CommentServiceTests
{
    [TestClass]
    public class GetAllComments_Should
    {
        [TestMethod]
        public void ReturnAllExistentEntitiesCount()
        {
            var options = Utilities.GetOptions(nameof(ReturnAllExistentEntitiesCount));

            var commentText = "commentTxt";
            var locationName = "locationName";
            var isHappy = true;
            var author = "testAuthor";
            var testCommentCount = 6;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                for (int i = 0; i < testCommentCount; i++)
                {
                    var location = arrangeContext.Locations.Add(new Location(locationName)).Entity;
                    arrangeContext.Comments.Add(new Comment(author, commentText, location, isHappy));
                }
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var commentService = new CommentService(assertContext, null);

                var sut = commentService.GetAllComments();

                Assert.IsTrue(sut.Count() == testCommentCount);
            }
        }

        [TestMethod]
        public void ReturnEmptyList_IfNoCommentsExist()
        {
            var options = Utilities.GetOptions(nameof(ReturnEmptyList_IfNoCommentsExist));

            var testCommentCount = 0;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var commentService = new CommentService(assertContext, null);

                var sut = commentService.GetAllComments();

                Assert.IsTrue(sut.Count() == testCommentCount);
            }
        }
    }
}
