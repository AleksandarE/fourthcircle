﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace FourthCircle.Services.Tests.CommentServiceTests
{
    [TestClass]
    public class EditComment_Should
    {
        [TestMethod]
        public void Succeed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WhenInputIsValid));

            var commentText = "commentTxt";
            var locationName = "locationName";
            var isHappy = true;
            var author = "testAuthor";
            var updatedCommentText = "commentTxt";
            string commentId;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var location = arrangeContext.Locations.Add(new Location(locationName)).Entity;
                var comment = arrangeContext.Comments.Add(new Comment(author, commentText, location, isHappy)).Entity;
                commentId = comment.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var commentService = new CommentService(assertContext, locationService);

                var sut = commentService.EditComment(commentId, updatedCommentText);

                Assert.IsTrue(sut.Author == author);
                Assert.IsTrue(sut.Text == updatedCommentText);
            }
        }

        [TestMethod]
        public void Throw_IfCommentDoesNotExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfCommentDoesNotExist));

            string commentId = null;
            var updatedCommentText = "commentTxt";

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var commentService = new CommentService(assertContext, locationService);

                Assert.ThrowsException<ArgumentNullException>(() => commentService.EditComment(commentId, updatedCommentText),
                    Constants.CommentNotFoundException);
            }
        }
    }
}
