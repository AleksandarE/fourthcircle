﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace FourthCircle.Services.Tests.CommentServiceTests
{
    [TestClass]
    public class CreateComment_Should
    {
        [TestMethod]
        public void Succeed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WhenInputIsValid));

            var commentText = "commentTxt";
            var locationName = "locationName";
            var isHappy = true;
            var author = "testAuthor";
            string locationId;
            Location location;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                location = arrangeContext.Locations.Add(new Location(locationName)).Entity;
                locationId = location.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var commentService = new CommentService(assertContext, locationService);

                var sut = commentService.CreateComment(commentText, locationName, isHappy, author);

                Assert.IsTrue(sut.Author == author);
                Assert.IsTrue(sut.Text == commentText);
                Assert.IsTrue(sut.IsHappy == isHappy);
                Assert.IsTrue(sut.LocationId == locationId);
            }
        }

        [TestMethod]
        public void Throw_IfLocationIsNull()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WhenInputIsValid));

            var commentText = "commentTxt";
            string locationName = null;
            var isHappy = true;
            var author = "testAuthor";

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var commentService = new CommentService(assertContext, locationService);

                Assert.ThrowsException<ArgumentException>(() => commentService.CreateComment(commentText, locationName, isHappy, author),
                    Constants.LocationNotProvidedException);
            }
        }

        [TestMethod]
        public void Throw_IfLocationDoesntExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfLocationDoesntExist));

            var commentText = "commentTxt";
            var testLocationName = "locationName";
            var isHappy = true;
            var author = "testAuthor";

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var commentService = new CommentService(assertContext, locationService);

                Assert.ThrowsException<ArgumentException>(() => commentService.CreateComment(commentText, testLocationName, isHappy, author),
                    string.Format(Constants.LocationNotFoundException,testLocationName));
            }
        }
    }
}
