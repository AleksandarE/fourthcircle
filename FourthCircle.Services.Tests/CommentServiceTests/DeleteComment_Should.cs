﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace FourthCircle.Services.Tests.CommentServiceTests
{
    [TestClass]
    public class DeleteComment_Should
    {
        [TestMethod]
        public void Succeed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WhenInputIsValid));

            var commentText = "commentTxt";
            var locationName = "locationName";
            var isHappy = true;
            var author = "testAuthor";
            string commentId;
            int testCommentCount;
            int deletedCommentCount;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var location = arrangeContext.Locations.Add(new Location(locationName)).Entity;
                var comment = arrangeContext.Comments.Add(new Comment(author, commentText, location, isHappy)).Entity;
                commentId = comment.Id;
                arrangeContext.SaveChanges();
                testCommentCount = arrangeContext.Comments.Count();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var commentService = new CommentService(assertContext, locationService);

                commentService.DeleteComment(commentId);
                deletedCommentCount = 1;

                Assert.IsTrue(assertContext.Comments.Find(commentId) == null);
                Assert.IsTrue(assertContext.Comments.Count() == testCommentCount - deletedCommentCount);
            }
        }

        [TestMethod]
        public void Throw_IfCommentDoesNotExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfCommentDoesNotExist));

            string commentId = null;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var commentService = new CommentService(assertContext, locationService);

                Assert.ThrowsException<ArgumentNullException>(() => commentService.DeleteComment(commentId),
                    Constants.CommentNotFoundException);
            }
        }
    }
}
