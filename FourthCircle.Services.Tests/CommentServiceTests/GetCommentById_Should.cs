﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace FourthCircle.Services.Tests.CommentServiceTests
{
    [TestClass]
    public class GetCommentById_Should
    {
        [TestMethod]
        public void Succeed_WhenInputIsValid()
        {
            var options = Utilities.GetOptions(nameof(Succeed_WhenInputIsValid));

            var commentText = "commentTxt";
            var locationName = "locationName";
            var isHappy = true;
            var author = "testAuthor";
            string commentId;

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var location = arrangeContext.Locations.Add(new Location(locationName)).Entity;
                var comment = arrangeContext.Comments.Add(new Comment(author, commentText, location, isHappy)).Entity;
                commentId = comment.Id;
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var commentService = new CommentService(assertContext, locationService);

                var sut = commentService.GetCommentById(commentId);

                Assert.IsTrue(sut.Id == commentId);
                Assert.IsTrue(sut.Text == commentText);
                Assert.IsTrue(sut.Author == author);
                Assert.IsTrue(sut.IsHappy == isHappy);
            }
        }

        [TestMethod]
        public void Throw_IfCommentDoesNotExist()
        {
            var options = Utilities.GetOptions(nameof(Throw_IfCommentDoesNotExist));

            string commentId = null;

            using (var assertContext = new ApplicationDbContext(options))
            {
                var locationService = new LocationService(assertContext);
                var commentService = new CommentService(assertContext, locationService);

                Assert.ThrowsException<ArgumentNullException>(() => commentService.GetCommentById(commentId),
                    Constants.CommentNotFoundException);
            }
        }
    }
}
