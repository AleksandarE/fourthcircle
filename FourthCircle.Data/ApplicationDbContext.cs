﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using FourthCircle.Data.Entities;
using FourthCircle.Data.Configurations;

namespace FourthCircle.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() {}

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Logbook> Logbooks { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<LogbooksUsers> LogbooksUsers { get; set; }
        public DbSet<LogbookCategories> LogbookCategories { get;set;}



        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new LogbooksUsersConfiguration());
            builder.ApplyConfiguration(new NotesCategoriesConfiguration());
            builder.ApplyConfiguration(new LogbookCategoriesConfiguration());
        }
    }
}
