﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace FourthCircle.Data.Entities
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            this.ChangePassword = true;
        }

        public ApplicationUser(string username) : base(username)
        {
            this.ChangePassword = true;
        }

        public bool ChangePassword { get; set; }
        public byte[] Avatar { get; set; }
        public ICollection<LogbooksUsers> LogbooksUsers { get; set; }
    }
}
