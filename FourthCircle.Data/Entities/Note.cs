﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FourthCircle.Data.Entities
{
    public class Note
    {
        public Note()
        {
            this.CreatedOn = DateTime.Now;
        }

        public Note(ApplicationUser user, string noteText, Logbook logbook, ICollection<NotesCategories> notesCategories = null)
        {
            User = user;
            Text = noteText;
            Logbook = logbook;
            CreatedOn = DateTime.Now;
            NotesCategories = notesCategories == null ? new List<NotesCategories>() : notesCategories;
        }

        [Key]
        public string Id { get; set; }
        [Required]
        [StringLength(280, MinimumLength = 5, ErrorMessage = "Please enter text between 5 and 280 characters.")]
        public string Text { get; set; }
        public ICollection<NotesCategories> NotesCategories { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public DateTime LastEditedOn { get; set; }
        public bool IsMarkedAsSolved { get; set; }
        public DateTime SolvedOn { get; set; }
        public string SolvedByUserId { get; set; }
        public ApplicationUser SolvedByUser { get; set; }
        public byte[] Picture { get; set; }
        public string LogbookId { get; set; }
        public Logbook Logbook { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
