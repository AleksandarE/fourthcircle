﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FourthCircle.Data.Entities
{
    public class NotesCategories
    {
        public NotesCategories()
        {

        }

        public NotesCategories(Note note, Category category)
        {
            Note = note;
            Category = category;
        }

        public string CategoryId { get; set; }
        public Category Category { get; set; }
        public string NoteId { get; set; }
        public Note Note { get; set; }
    }
}
