﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FourthCircle.Data.Entities
{
    public class Comment
    {
        public Comment()
        {

        }
        public Comment(string authorName, string commentText, Location location, bool? isHappy)
        {
            this.Author = authorName;
            this.Text = commentText;
            this.Location = location;
            this.CreatedOn = DateTime.Now;
            this.IsHappy = isHappy;
        }

        public string Id { get; set; }
        [Required]
        [StringLength(280, MinimumLength = 5, ErrorMessage = "Please enter text between 5 and 280 characters.")]
        public string Text { get; set; }
        [Required]
        public string Author { get; set; }
        public string LocationId { get; set; }
        [Required]
        public Location Location { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; }
        public bool? IsHappy { get; set; }
    }
}
