﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FourthCircle.Data.Entities
{
    public class LogbooksUsers
    {
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string LogbookId { get; set; }
        public Logbook Logbook { get; set; }
    }
}
