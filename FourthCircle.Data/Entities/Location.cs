﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FourthCircle.Data.Entities
{
    public class Location
    {
        public Location(string name)
        {
            this.Name = name;
            this.Lattitude = "51.123420";
            this.Longtitude= "4.141920";
            Logbooks = new List<Logbook>();
        }

        [Key]
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Lattitude { get; set; }
        [Required]
        public string Longtitude { get; set; }

        public ICollection<Logbook> Logbooks { get; set; }
        public ICollection<Comment> Comments { get;set;}
    }
}