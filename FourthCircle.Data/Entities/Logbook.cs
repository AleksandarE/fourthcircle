﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FourthCircle.Data.Entities
{
    public class Logbook
    {
        public Logbook(string name)
        {
            this.Name = name;
            LogbookCategories = new List<LogbookCategories>();
            LogbooksUsers = new List<LogbooksUsers>();
            Notes = new List<Note>();
        }
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string LocationId { get; set; }
        public Location Location { get; set; }
        public ICollection<LogbookCategories> LogbookCategories { get; set; } 
        public ICollection<LogbooksUsers> LogbooksUsers { get; set; }
        public ICollection<Note> Notes { get; set; }
    }
}