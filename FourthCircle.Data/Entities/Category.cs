﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FourthCircle.Data.Entities
{
    public class Category
    {
        public Category(string name)
        {
            this.Name = name;
            this.NotesCategories = new List<NotesCategories>();
            this.LogbookCategories = new List<LogbookCategories>();
        }
        [Key]
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        public ICollection<NotesCategories> NotesCategories { get; set; }
        public ICollection<LogbookCategories> LogbookCategories { get; set; }

    }
}
