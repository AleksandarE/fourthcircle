﻿using System;
namespace FourthCircle.Data.Entities
{
    public class LogbookCategories
    {
        public string CategoryId { get; set; }
        public Category Category { get; set; }
        public string LogbookId { get; set; }
        public Logbook Logbook { get; set; }
    }
}

