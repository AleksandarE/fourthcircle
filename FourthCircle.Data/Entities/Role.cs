﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FourthCircle.Data.Entities
{
    public class Role
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
