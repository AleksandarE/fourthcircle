﻿using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FourthCircle.Data
{
    public class DatabaseSeeder
    {
        private readonly ApplicationDbContext dbContext;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public DatabaseSeeder(
            ApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)

        {
            this.dbContext = dbContext;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public async Task Invoke()
        {
            await this.AddRolesAsync();
            await this.AddAdminAsync();
        }

        private async Task AddRolesAsync()
        {
            string[] roleNames = { "Admin", "Manager", "Moderator" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await this.roleManager.RoleExistsAsync(roleName);
                if (!roleExist) roleResult = await this.roleManager.CreateAsync(new IdentityRole(roleName));
            }
        }

        private async Task AddAdminAsync()
        {
            if (dbContext.Users.Any(u => u.UserName == "Admin")) return;

            var admin = new ApplicationUser
            {
                UserName = "Admin",
                Email = "admin@admin.com"
            };
            var adminPassword = "Admin1@";

            var createPowerUser = await this.userManager.CreateAsync(admin, adminPassword);
            if (createPowerUser.Succeeded) await userManager.AddToRoleAsync(admin, "Admin");
        }

    }
}
