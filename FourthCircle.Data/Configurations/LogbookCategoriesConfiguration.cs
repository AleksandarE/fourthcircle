﻿using System;
using FourthCircle.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FourthCircle.Data.Configurations
{
    public class LogbookCategoriesConfiguration : IEntityTypeConfiguration<LogbookCategories>
    {
        public void Configure(EntityTypeBuilder<LogbookCategories> builder)
        {
            builder.HasKey(b => new { b.LogbookId, b.CategoryId});
        }
    }
}
