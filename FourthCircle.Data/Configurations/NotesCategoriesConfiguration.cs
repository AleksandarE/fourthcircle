﻿using FourthCircle.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FourthCircle.Data.Configurations
{
    public class NotesCategoriesConfiguration : IEntityTypeConfiguration<NotesCategories>
    {
        public void Configure(EntityTypeBuilder<NotesCategories> builder)
        {
            builder.HasKey(b => new { b.NoteId, b.CategoryId });
        }
    }
}
