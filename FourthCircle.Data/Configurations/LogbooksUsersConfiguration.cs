﻿using FourthCircle.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FourthCircle.Data.Configurations
{
    public class LogbooksUsersConfiguration : IEntityTypeConfiguration<LogbooksUsers>
    {
        public void Configure(EntityTypeBuilder<LogbooksUsers> builder)
        {
            builder.HasKey(b => new { b.LogbookId, b.UserId });
        }
    }
}
