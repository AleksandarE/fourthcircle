﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FourthCircle.Data.Migrations
{
    public partial class Updated_Note : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsMarkedAsSolved",
                table: "Notes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastEditedOn",
                table: "Notes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "SolvedByUserId",
                table: "Notes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SolvedOn",
                table: "Notes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_Notes_SolvedByUserId",
                table: "Notes",
                column: "SolvedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_AspNetUsers_SolvedByUserId",
                table: "Notes",
                column: "SolvedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notes_AspNetUsers_SolvedByUserId",
                table: "Notes");

            migrationBuilder.DropIndex(
                name: "IX_Notes_SolvedByUserId",
                table: "Notes");

            migrationBuilder.DropColumn(
                name: "IsMarkedAsSolved",
                table: "Notes");

            migrationBuilder.DropColumn(
                name: "LastEditedOn",
                table: "Notes");

            migrationBuilder.DropColumn(
                name: "SolvedByUserId",
                table: "Notes");

            migrationBuilder.DropColumn(
                name: "SolvedOn",
                table: "Notes");
        }
    }
}
