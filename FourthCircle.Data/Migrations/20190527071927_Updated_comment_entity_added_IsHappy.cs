﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FourthCircle.Data.Migrations
{
    public partial class Updated_comment_entity_added_IsHappy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsHappy",
                table: "Comments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsHappy",
                table: "Comments");
        }
    }
}
