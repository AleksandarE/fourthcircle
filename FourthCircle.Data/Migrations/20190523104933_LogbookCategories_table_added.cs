﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FourthCircle.Data.Migrations
{
    public partial class LogbookCategories_table_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Categories_Logbooks_LogbookId",
                table: "Categories");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Logbooks_LogbookId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_LogbooksUsers_Logbooks_LogbookID",
                table: "LogbooksUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_LogbooksUsers_AspNetUsers_UserID",
                table: "LogbooksUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Notes_Locations_LocationID",
                table: "Notes");

            migrationBuilder.DropForeignKey(
                name: "FK_Notes_Logbooks_LogbookID",
                table: "Notes");

            migrationBuilder.DropForeignKey(
                name: "FK_Notes_AspNetUsers_UserID",
                table: "Notes");

            migrationBuilder.DropForeignKey(
                name: "FK_NotesCategories_Categories_CategoryId",
                table: "NotesCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_NotesCategories_Notes_NoteID",
                table: "NotesCategories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NotesCategories",
                table: "NotesCategories");

            migrationBuilder.DropIndex(
                name: "IX_Categories_LogbookId",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "CategryID",
                table: "NotesCategories");

            migrationBuilder.DropColumn(
                name: "LogbookId",
                table: "Categories");

            migrationBuilder.RenameColumn(
                name: "NoteID",
                table: "NotesCategories",
                newName: "NoteId");

            migrationBuilder.RenameColumn(
                name: "UserID",
                table: "Notes",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "LogbookID",
                table: "Notes",
                newName: "LogbookId");

            migrationBuilder.RenameColumn(
                name: "LocationID",
                table: "Notes",
                newName: "LocationId");

            migrationBuilder.RenameIndex(
                name: "IX_Notes_UserID",
                table: "Notes",
                newName: "IX_Notes_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Notes_LogbookID",
                table: "Notes",
                newName: "IX_Notes_LogbookId");

            migrationBuilder.RenameIndex(
                name: "IX_Notes_LocationID",
                table: "Notes",
                newName: "IX_Notes_LocationId");

            migrationBuilder.RenameColumn(
                name: "UserID",
                table: "LogbooksUsers",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "LogbookID",
                table: "LogbooksUsers",
                newName: "LogbookId");

            migrationBuilder.RenameIndex(
                name: "IX_LogbooksUsers_UserID",
                table: "LogbooksUsers",
                newName: "IX_LogbooksUsers_UserId");

            migrationBuilder.RenameColumn(
                name: "LogbookId",
                table: "Comments",
                newName: "LocationId");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_LogbookId",
                table: "Comments",
                newName: "IX_Comments_LocationId");

            migrationBuilder.AlterColumn<string>(
                name: "CategoryId",
                table: "NotesCategories",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_NotesCategories",
                table: "NotesCategories",
                columns: new[] { "NoteId", "CategoryId" });

            migrationBuilder.CreateTable(
                name: "LogbookCategories",
                columns: table => new
                {
                    CategoryId = table.Column<string>(nullable: false),
                    LogbookId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogbookCategories", x => new { x.LogbookId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_LogbookCategories_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LogbookCategories_Logbooks_LogbookId",
                        column: x => x.LogbookId,
                        principalTable: "Logbooks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LogbookCategories_CategoryId",
                table: "LogbookCategories",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Locations_LocationId",
                table: "Comments",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LogbooksUsers_Logbooks_LogbookId",
                table: "LogbooksUsers",
                column: "LogbookId",
                principalTable: "Logbooks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LogbooksUsers_AspNetUsers_UserId",
                table: "LogbooksUsers",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_Locations_LocationId",
                table: "Notes",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_Logbooks_LogbookId",
                table: "Notes",
                column: "LogbookId",
                principalTable: "Logbooks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_AspNetUsers_UserId",
                table: "Notes",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NotesCategories_Categories_CategoryId",
                table: "NotesCategories",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NotesCategories_Notes_NoteId",
                table: "NotesCategories",
                column: "NoteId",
                principalTable: "Notes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Locations_LocationId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_LogbooksUsers_Logbooks_LogbookId",
                table: "LogbooksUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_LogbooksUsers_AspNetUsers_UserId",
                table: "LogbooksUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Notes_Locations_LocationId",
                table: "Notes");

            migrationBuilder.DropForeignKey(
                name: "FK_Notes_Logbooks_LogbookId",
                table: "Notes");

            migrationBuilder.DropForeignKey(
                name: "FK_Notes_AspNetUsers_UserId",
                table: "Notes");

            migrationBuilder.DropForeignKey(
                name: "FK_NotesCategories_Categories_CategoryId",
                table: "NotesCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_NotesCategories_Notes_NoteId",
                table: "NotesCategories");

            migrationBuilder.DropTable(
                name: "LogbookCategories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NotesCategories",
                table: "NotesCategories");

            migrationBuilder.RenameColumn(
                name: "NoteId",
                table: "NotesCategories",
                newName: "NoteID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Notes",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "LogbookId",
                table: "Notes",
                newName: "LogbookID");

            migrationBuilder.RenameColumn(
                name: "LocationId",
                table: "Notes",
                newName: "LocationID");

            migrationBuilder.RenameIndex(
                name: "IX_Notes_UserId",
                table: "Notes",
                newName: "IX_Notes_UserID");

            migrationBuilder.RenameIndex(
                name: "IX_Notes_LogbookId",
                table: "Notes",
                newName: "IX_Notes_LogbookID");

            migrationBuilder.RenameIndex(
                name: "IX_Notes_LocationId",
                table: "Notes",
                newName: "IX_Notes_LocationID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "LogbooksUsers",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "LogbookId",
                table: "LogbooksUsers",
                newName: "LogbookID");

            migrationBuilder.RenameIndex(
                name: "IX_LogbooksUsers_UserId",
                table: "LogbooksUsers",
                newName: "IX_LogbooksUsers_UserID");

            migrationBuilder.RenameColumn(
                name: "LocationId",
                table: "Comments",
                newName: "LogbookId");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_LocationId",
                table: "Comments",
                newName: "IX_Comments_LogbookId");

            migrationBuilder.AlterColumn<string>(
                name: "CategoryId",
                table: "NotesCategories",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "CategryID",
                table: "NotesCategories",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "LogbookId",
                table: "Categories",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_NotesCategories",
                table: "NotesCategories",
                columns: new[] { "NoteID", "CategryID" });

            migrationBuilder.CreateIndex(
                name: "IX_Categories_LogbookId",
                table: "Categories",
                column: "LogbookId");

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_Logbooks_LogbookId",
                table: "Categories",
                column: "LogbookId",
                principalTable: "Logbooks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Logbooks_LogbookId",
                table: "Comments",
                column: "LogbookId",
                principalTable: "Logbooks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LogbooksUsers_Logbooks_LogbookID",
                table: "LogbooksUsers",
                column: "LogbookID",
                principalTable: "Logbooks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LogbooksUsers_AspNetUsers_UserID",
                table: "LogbooksUsers",
                column: "UserID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_Locations_LocationID",
                table: "Notes",
                column: "LocationID",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_Logbooks_LogbookID",
                table: "Notes",
                column: "LogbookID",
                principalTable: "Logbooks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_AspNetUsers_UserID",
                table: "Notes",
                column: "UserID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NotesCategories_Categories_CategoryId",
                table: "NotesCategories",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NotesCategories_Notes_NoteID",
                table: "NotesCategories",
                column: "NoteID",
                principalTable: "Notes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
