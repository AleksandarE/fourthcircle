﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;


namespace FourthCircle.Services
{
    public class LocationService : ILocationService
    {
        private readonly ApplicationDbContext dbContext;

        public LocationService(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public Location CreateLoctation(string locationName)
        {
            if (LocationExists(locationName))
            {
                throw new ArgumentException(string.Format(Constants.LocationAlreadyExistsException, locationName));
            }

            var newLocation = new Location(locationName);
            this.dbContext.Locations.Add(newLocation);
            this.dbContext.SaveChanges();
            return newLocation;
        }

        public Location GetLocation(string locationName)
        {
            return this.dbContext.Locations
                .Include(l => l.Logbooks)
                .Include(l => l.Comments)
                .FirstOrDefault(x => x.Name == locationName);
        }

        public ICollection<string> GetLocationNames()
        {
            return this.dbContext.Locations.Select(x => x.Name).ToList();
        }

        private bool LocationExists(string locationName)
        {
            return this.dbContext.Locations.Any(x => x.Name == locationName);
        }

        public Location GetLocationById(string id)
        {
            var location = this.dbContext.Locations.FirstOrDefault(l => l.Id == id);
            return location;
        }

        public IQueryable<Location> GetLocationsByUser(string userId)
        {
            return this.dbContext.Locations
                .Include(l => l.Logbooks)
                .Where(l => l.Logbooks.Any(lb => lb.LogbooksUsers.Any(lu => lu.UserId == userId)));
            //.Include(l => l.Logbooks)
            //.Where(l => l.Logbooks.Where(u => u.LogbooksUsers.Any(lu => lu.UserId == userId)));
        }
    }
}
