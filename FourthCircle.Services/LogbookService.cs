﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services
{
    public class LogbookService : ILogbookService
    {
        private readonly ApplicationDbContext dBcontext;
        private readonly ILocationService locationService;

        public LogbookService(ApplicationDbContext dBcontext, ILocationService locationService)
        {
            this.dBcontext = dBcontext;
            this.locationService = locationService;
        }

        public Logbook CreateLogbook(string logbookName, string locationName, ICollection<string> categoriesToAssign, ICollection<string> managersToAssign)
        {
            if (LogbookExistsAtLocation(logbookName, locationName))
            {
                throw new ArgumentException(string.Format(Constants.LocationContainsLogbookWithSameNameException, locationName, logbookName));
            }

            var newLogbook = new Logbook(logbookName);

            var targetLocation = locationService.GetLocation(locationName)
                ?? locationService.CreateLoctation(locationName);

            newLogbook.LocationId = targetLocation.Id;
            newLogbook.Location = targetLocation;
            this.dBcontext.Logbooks.Add(newLogbook);
            this.dBcontext.SaveChanges();

            AssignManagers(newLogbook.Id, managersToAssign);
            AssignCategories(newLogbook.Id, categoriesToAssign);


            return newLogbook;
        }

        public IQueryable<Logbook> GetAllLogbooks()
        {
            return this.dBcontext.Logbooks
                .Include(x => x.Location)
                .Include(x => x.LogbookCategories)
                    .ThenInclude(lc => lc.Category)
                .Include(x => x.LogbooksUsers)
                    .ThenInclude(lu => lu.User)
                    .Include(x => x.Notes).AsNoTracking();
        }

        public IQueryable<Logbook> GetLogbooksByUser(string userId)
        {
            return this.dBcontext.Logbooks.Where(l => l.LogbooksUsers.Any(u => u.UserId == userId))
                .Include(x => x.Location)
                .Include(x => x.LogbookCategories)
                    .ThenInclude(lc => lc.Category)
                .Include(x => x.LogbooksUsers)
                    .ThenInclude(lu => lu.User)
                    .Include(x => x.Notes);
        }

        public Logbook AssignCategories(string logbookId, ICollection<string> categoriesToAssign)
        {
            var targetLogbook = GetLogbook(logbookId) ?? throw new ArgumentException(string.Format(Constants.LogbookNotFoundException, logbookId));

            targetLogbook.LogbookCategories.Clear();

            foreach (var item in categoriesToAssign)
            {
                targetLogbook.LogbookCategories.Add(new LogbookCategories { Logbook = targetLogbook, Category = this.dBcontext.Categories.FirstOrDefault(x => x.Name == item) });
            }

            dBcontext.SaveChanges();

            return targetLogbook;
        }

        public Logbook AssignManagers(string logbookId, ICollection<string> managersToAssign)
        {
            var targetLogbook = GetLogbook(logbookId) ?? throw new ArgumentException(string.Format(Constants.LogbookNotFoundException, logbookId));

            targetLogbook.LogbooksUsers.Clear();

            foreach (var item in managersToAssign)
            {
                targetLogbook.LogbooksUsers.Add(new LogbooksUsers() { Logbook = targetLogbook, User = this.dBcontext.Users.FirstOrDefault(x => x.UserName == item) });
            }

            this.dBcontext.SaveChanges();

            return targetLogbook;
        }

        public Logbook GetLogbook(string Id)
        {
            return this.dBcontext.Logbooks
                .Include(x => x.Location)
                .Include(x => x.LogbookCategories)
                    .ThenInclude(lc => lc.Category)
                .Include(x => x.LogbooksUsers)
                    .ThenInclude(lu => lu.User)
                .Include(x => x.Notes)
                    .ThenInclude(x => x.NotesCategories)
                        .ThenInclude(x => x.Category)
                .FirstOrDefault(x => x.Id == Id);
        }

        private bool LogbookExistsAtLocation(string logbookName, string locationName)
        {
            return this.dBcontext.Locations
                .Include(x => x.Logbooks)
                .Where(x => x.Name == locationName)
                .Select(x => x.Logbooks
                    .FirstOrDefault(y => y.Name == logbookName))
                        .FirstOrDefault() != null;
        }

        public void DeleteLogbook(Logbook logbookToDelete)
        {
            this.dBcontext.Logbooks.Remove(logbookToDelete);
            this.dBcontext.SaveChanges();
        }

        public IQueryable<Logbook> FilterLogbooks(string sortOrder, string searchString, string userSearch)
        {
            var logbooks = GetAllLogbooks();

            if (!string.IsNullOrEmpty(searchString))
            {
                logbooks = logbooks.Where(l => l.Location.Name.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(userSearch))
            {
                logbooks = logbooks.Where(x => x.LogbooksUsers.Select(y => y.User).Any(u => u.UserName.Contains(userSearch)));
            }

            switch (sortOrder)
            {
                case "location_desc":
                    logbooks = logbooks.OrderByDescending(l => l.Location.Name);
                    break;
                case "logbook_desc":
                    logbooks = logbooks.OrderByDescending(l => l.Name);
                    break;
                case "logbook_name":
                    logbooks = logbooks.OrderBy(l => l.Name);
                    break;
                default:
                    logbooks = logbooks.OrderBy(l => l.Location.Name);
                    break;
            }

            return logbooks;
        }
    }
}
