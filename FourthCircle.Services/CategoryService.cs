﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ApplicationDbContext dBcontext;

        public CategoryService(ApplicationDbContext dBcontext)
        {
            this.dBcontext = dBcontext;
        }

        public ICollection<Category> GetAllCategories()
        {
            return this.dBcontext.Categories.ToList();
        }

        public Category CreateCategory(string categoryName)
        {
            if (CategoryExists(categoryName))
            {
                throw new ArgumentException(string.Format(Constants.CategoryExistsException, categoryName));
            }

            var newCategory = categoryName != null ?
                new Category(categoryName) :
                throw new ArgumentException(Constants.CategoryNameMissingException);
            this.dBcontext.Categories.Add(newCategory);
            dBcontext.SaveChanges();
            return newCategory;
        }

        private bool CategoryExists(string categoryName)
        {
            return this.dBcontext.Categories.Any(x => x.Name == categoryName);
        }
    }
}
