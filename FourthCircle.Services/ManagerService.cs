﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FourthCircle.Services
{
    public class ManagerService : IManagerService
    {
        private readonly ApplicationDbContext dbContext;
        private readonly ILogbookService logbookService;
        private readonly IUserService userService;

        public ManagerService(ApplicationDbContext dbContext, ILogbookService logbookService, IUserService userService)
        {
            this.dbContext = dbContext;
            this.logbookService = logbookService;
            this.userService = userService;
        }

        public Note AddCommentAsNote(string userId, /*string customerName,*/ string commentText, string logbookId, ICollection<string> categories = null, byte[] picture = null)
        {
            //var text = commentText.Insert(0, $"Customer comment from \"{customerName}\":\n");

            var note = AddNote(userId, commentText, logbookId, categories, null); // text

            return note;
        }

        public Note AddNote(string userId, string text, string logbookId, ICollection<string> categories = null, IFormFile uploadedPicture = null)
        {
            var logbook = logbookService.GetLogbook(logbookId) ??
                throw new ArgumentException(string.Format(Constants.LogbookNotFoundException, logbookId));
            var user = userService.GetUserById(userId);

            byte[] picture = null;
            if (uploadedPicture != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    uploadedPicture.CopyTo(memoryStream);
                    picture = memoryStream.ToArray();
                }
            }

            var note = new Note
            {
                User = user,
                Text = text,
                Logbook = logbook,
                NotesCategories = new List<NotesCategories>(),
                Picture = picture
            };

            if (categories != null)
            {
                foreach (var categoryName in categories)
                {
                    var category = GetLogbookCategory(logbook, categoryName) ??
                        throw new ArgumentException(string.Format(Constants.CategoryWithNameDoesNotExistException, categoryName)); //TODO add new category instead?
                    note.NotesCategories.Add(new NotesCategories { Note = note, Category = category });
                }
            }
            dbContext.Notes.Add(note);
            dbContext.SaveChanges();

            return note;
        }

        public Note EditNote(string userId, string noteId, string logbookId, string text, ICollection<string> categories = null)
        {
            var note = GetNoteById(noteId);//, logbookId);

            if (note.UserId != userId)
            {
                throw new ArgumentException(
                    Constants.UserIDMissmatchException);
            }

            var logbook = logbookService.GetLogbook(logbookId) ??
                throw new ArgumentException(string.Format(Constants.LogbookNotFoundException, logbookId)); ;

            if (categories != null)
            {
                note.NotesCategories.Clear();
                foreach (var category in categories)
                {
                    note.NotesCategories.Add(new NotesCategories { Note = note, Category = GetLogbookCategory(logbook, category) });
                }
            }

            note.Text = text;
            note.LastEditedOn = DateTime.Now;

            dbContext.SaveChanges();

            return note;
        }

        public Note DeleteNote(string userId, string noteId, string logbookId)
        {
            var note = GetNoteById(noteId);//, logbookId);
            if (note.UserId != userId)
            {
                throw new ArgumentException(
                    Constants.UserIDMissmatchException);
            }

            dbContext.Notes.Remove(note);
            dbContext.SaveChanges();

            return note;
        }

        public Note MarkNoteAsSolved(string userId, string noteId, string logbookId)
        {
            var note = GetNoteById(noteId);//, logbookId);
            ApplicationUser user;

            try
            {
                user = userService.GetUserById(userId);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException(string.Format(Constants.UserWithIdNotFoundException, userId));
            }

            note.IsMarkedAsSolved = true;
            note.SolvedOn = DateTime.Now;
            note.SolvedByUser = user;

            dbContext.SaveChanges();

            return note;
        }

        public IEnumerable<Note> FilterLogbookNotes(string logbookId, string category = null, string searchString = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var notes = (category == null) ? ListNotes(logbookId) : ListNotesByCategory(logbookId, category);

            if (!string.IsNullOrEmpty(searchString))
            {
                notes = notes.Where(x => x.Text.IndexOf(searchString, StringComparison.CurrentCultureIgnoreCase) != -1);
            }

            if (!(dateFrom == null))
            {
                notes = notes.Where(n => n.CreatedOn > dateFrom);
            }

            if (!(dateTo == null))
            {
                notes = notes.Where(n => n.CreatedOn < dateTo);
            }

            return notes.OrderByDescending(n => n.CreatedOn).ToList();
        }

        public IEnumerable<Note> ListNotes(string logbookId)
        {
            if (dbContext.Logbooks.Find(logbookId) == null)
            {
                throw new ArgumentException(string.Format(Constants.LogbookNotFoundException, logbookId));
            }
            ICollection<Note> notes = dbContext.Notes.Where(n => n.LogbookId == logbookId)
                    .Include(n => n.User)
                    .Include(n => n.SolvedByUser)
                    .Include(n => n.NotesCategories)
                        .ThenInclude(n => n.Category)
                    .ToList();

            return notes;
        }

        public IEnumerable<Category> GetLogbookCategories(string logbookId)
        {
            var categories = dbContext.Categories.Where(c => c.LogbookCategories.Any(lc => lc.LogbookId == logbookId)).ToList();
            return categories;
        }

        public IEnumerable<string> GetCategoryListAsString(string logbookId)
        {
            var categoryQry = GetLogbookCategories(logbookId);
            var categoryLst = categoryQry.Distinct().Select(c => c.Name);
            return categoryLst;
        }

        public Note GetNoteById(string noteId)//, string logbookId)
        {
            return dbContext.Notes
                .Include(n => n.NotesCategories)
                .FirstOrDefault(n => n.Id == noteId) ?? throw new ArgumentException(
                    string.Format(Constants.NoteNotFoundException, noteId)); ;
        }

        private IEnumerable<Note> ListNotesByCategory(string logbookId, string categoryName)
        {
            var notes = dbContext.Notes.Where(n => n.NotesCategories.Any(nc => nc.Category.Name == categoryName && nc.Note.LogbookId == logbookId))
                .Include(n => n.User)
                    .Include(n => n.SolvedByUser)
                    .Include(n => n.NotesCategories)
                        .ThenInclude(n => n.Category)
                    .ToList();

            return notes;
        }

        private Category GetLogbookCategory(Logbook logbook, string categoryName)
        {
            var categories = dbContext.Categories.Where(c => c.LogbookCategories.Any(lc => lc.Logbook == logbook)).ToList();
            return categories.FirstOrDefault(c => c.Name == categoryName);
        }
    }
}
