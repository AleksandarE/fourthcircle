﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FourthCircle.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext dBcontext;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public UserService()
        {
        }

        public UserService(ApplicationDbContext dBcontext, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.dBcontext = dBcontext;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public bool ShouldChangePassword(string username)
        {
            var targetUser = GetUserByName(username);
            return this.dBcontext.Users.Find(targetUser.Id).ChangePassword;
        }

        public ApplicationUser GetUserByName(string userName)
        {
            return this.dBcontext.Users.FirstOrDefault(x => x.UserName == userName);
        }

        public bool SuccessfulPasswordChange(ApplicationUser user)
        {
            user.ChangePassword = false;
            this.dBcontext.Users.Update(user);
            this.dBcontext.SaveChanges();
            return true;
        }

        public async Task<ApplicationUser> ChangeRole(string Id, string roleName)
        {
            var user = GetUserById(Id);
            var roles = await userManager.GetRolesAsync(user);

            if (roles[0] == "Manager")
            {
                RevokeManagerAccessToLogbooks(user);
            }

            foreach (var item in roles)
            {
                await this.userManager.RemoveFromRoleAsync(user, item);
            }
            await this.userManager.AddToRoleAsync(user, roleName);
            this.dBcontext.SaveChanges();

            return user;
        }

        private bool UserExists(string Id)
        {
            return GetUserById(Id) != null;
        }

        public void DeleteUser(string Id)
        {
            var user = GetUserById(Id);
            this.dBcontext.Users.Remove(user);
            this.dBcontext.SaveChanges();
        }

        public IQueryable<ApplicationUser> GetAllUsers()
        {
            return this.dBcontext.Users
                .Include(x => x.LogbooksUsers)
                    .ThenInclude(lu => lu.Logbook)
                .OrderBy(x => x.UserName).AsNoTracking();
        }

        public ApplicationUser GetUserById(string Id)
        {
            return this.dBcontext.Users.Find(Id) ?? throw new ArgumentException(string.Format(Constants.UserWithIdNotFoundException, Id));
        }

        public async Task<ICollection<string>> GetUsernamesInRole(string roleName)
        {
            var managers = await userManager.GetUsersInRoleAsync(roleName);
            return managers.Select(x => x.UserName).ToList();
        }

        public ApplicationUser RevokeManagerAccessToLogbooks(ApplicationUser user)
        {
            var logbooksForUser = this.dBcontext.LogbooksUsers.Where(x => x.User.Id == user.Id).ToList();

            foreach (var item in logbooksForUser)
            {
                this.dBcontext.LogbooksUsers.Remove(item);
            }
            this.dBcontext.SaveChanges();

            return user;
        }

        public IQueryable<ApplicationUser> FilterUsers(string sortOrder, string searchString)
        {
            var users = GetAllUsers();

            if (!string.IsNullOrEmpty(searchString))
            {
                users = users.Where(s => s.UserName.Contains(searchString) || s.Email.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    users = users.OrderByDescending(s => s.UserName);
                    break;
                default:
                    users = users.OrderBy(s => s.UserName);
                    break;
            }

            return users;
        }

        public List<SelectListItem> GetAvailableRoles()
        {
            List<SelectListItem> availableRoles = new List<SelectListItem>();
            foreach (var item in roleManager.Roles)
            {
                availableRoles.Add(new SelectListItem { Value = item.Name, Text = item.Name });
            }
            return availableRoles;
        }

        public byte[] ChangeAvatar(string userId, IFormFile newAvatar)
        {
            var user = GetUserById(userId);

            if (newAvatar != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    newAvatar.CopyTo(memoryStream); // await CopyToAsync
                    user.Avatar = memoryStream.ToArray();
                }
            }

            this.dBcontext.SaveChanges();
            return user.Avatar;
        }

        public byte[] GetUserAvatar(string userId)
        {
            return GetUserById(userId).Avatar;
        }
    }
}
