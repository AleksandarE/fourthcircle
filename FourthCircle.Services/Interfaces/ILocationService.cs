﻿using FourthCircle.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services.Interfaces
{
    public interface ILocationService
    {
        Location CreateLoctation(string name);
        Location GetLocation(string locationName);
        ICollection<string> GetLocationNames();
        Location GetLocationById(string Id);
        IQueryable<Location> GetLocationsByUser(string userId);
    }
}
