﻿using FourthCircle.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace FourthCircle.Services.Interfaces
{
    public interface ILogbookService
    {
        Logbook CreateLogbook(string name, string locationName, ICollection<string> categoriesToAssign, ICollection<string> managersToAssign);
        IQueryable<Logbook> GetAllLogbooks();
        Logbook AssignCategories(string logbookId, ICollection<string> categoriesToAssign);
        Logbook AssignManagers(string logbookId, ICollection<string> managersToAssign);
        IQueryable<Logbook> GetLogbooksByUser(string userId);
        Logbook GetLogbook(string Id);
        void DeleteLogbook(Logbook logbookToDelete);
        IQueryable<Logbook> FilterLogbooks(string sortOrder, string searchString, string userSearch);
    }
}
