﻿using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FourthCircle.Services.Interfaces
{
    public interface IUserService
    {
        bool ShouldChangePassword(string id);
        ApplicationUser GetUserByName(string userName);
        bool SuccessfulPasswordChange(ApplicationUser user);
        Task<ApplicationUser> ChangeRole(string id, string roleName);
        void DeleteUser(string Id);
        IQueryable<ApplicationUser> GetAllUsers();
        ApplicationUser GetUserById(string Id);
        Task<ICollection<string>> GetUsernamesInRole(string roleName);
        IQueryable<ApplicationUser> FilterUsers(string sortOrder, string searchString);
        List<SelectListItem> GetAvailableRoles();
        byte[] ChangeAvatar(string userId, IFormFile newAvatar);
        byte[] GetUserAvatar(string userId);
    }
}
