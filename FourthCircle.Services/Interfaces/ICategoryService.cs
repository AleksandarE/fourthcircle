﻿using FourthCircle.Data.Entities;
using System.Collections.Generic;

namespace FourthCircle.Services.Interfaces
{
    public interface ICategoryService
    {
        ICollection<Category> GetAllCategories();
        Category CreateCategory(string name);
    }
}
