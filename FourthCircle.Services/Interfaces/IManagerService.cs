﻿using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace FourthCircle.Services.Interfaces
{
    public interface IManagerService
    {
        Note AddNote(string userId, string text, string logbookId, ICollection<string> categories = null, IFormFile uploadedPicture = null);
        Note AddCommentAsNote(string userId, /*string customerName, */string commentText, string logbookId, ICollection<string> categories = null, byte[] picture = null);
        Note GetNoteById(string noteId);
        IEnumerable<Note> ListNotes(string logbookId);
        IEnumerable<Note> FilterLogbookNotes(string logbookId, string category = null, string searchString = null, DateTime? dateFrom = null, DateTime? dateTo = null);
        IEnumerable<Category> GetLogbookCategories(string logbookId);
        IEnumerable<string> GetCategoryListAsString(string logbookId);
        Note EditNote(string userId, string noteId, string logbookId, string text, ICollection<string> categories = null);
        Note DeleteNote(string userId, string noteId, string logbookId);
        Note MarkNoteAsSolved(string userId, string noteId, string logbookId);
    }
}