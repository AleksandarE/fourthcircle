﻿using FourthCircle.Data.Entities;
using System.Linq;

namespace FourthCircle.Services.Interfaces
{
    public interface ICommentService
    {
        Comment CreateComment(string commentText, string locationName, bool? isHappy, string author = "Anonymous");
        IQueryable<Comment> GetAllComments();
        Comment EditComment(string commentId, string updatedText);
        void DeleteComment(string commentId);
        Comment GetCommentById(string commentId);
    }
}
