﻿namespace FourthCircle.Services
{
    public static class Constants
    {
        //CategoryService
        public static string CategoryExistsException = "Category with name {0} already exists";
        public static string CategoryNameMissingException = "Category name cannot be empty.";

        //CommentService
        public static string LocationNotProvidedException = "Location cannot be null.";
        public static string LocationNotFoundException = "Couldn't find location with name {0}";
        public static string CommentNotFoundException = "Invalid comment ID.";

        //LocationService
        public static string LocationAlreadyExistsException = "Location with name {0} already exists.";

        //LogbookService
        public static string LocationContainsLogbookWithSameNameException = "{0} already contains logbook with name {1}.";
        public static string LogbookNotFoundException = "Logbook with Id '{0}' does note exist in the database.";

        //ManagerService
        public static string CategoryWithNameDoesNotExistException = "A category with name {0} does not exist";
        public static string UserIDMissmatchException = "User ID doesn't match the User ID of note's author";
        public static string UserWithIdNotFoundException = "User with Id {0} does not exist in the database";
        public static string NoteNotFoundException = "A Note with ID '{0}' does note exist in the database.";

    }
}
