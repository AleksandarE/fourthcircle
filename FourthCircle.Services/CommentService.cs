﻿using FourthCircle.Data;
using FourthCircle.Data.Entities;
using FourthCircle.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace FourthCircle.Services
{
    public class CommentService : ICommentService
    {
        private readonly ApplicationDbContext dBContext;
        private readonly ILocationService locationService;

        public CommentService(ApplicationDbContext dBContext, ILocationService locationService)
        {
            this.dBContext = dBContext;
            this.locationService = locationService;
        }

        public Comment CreateComment(string commentText, string locationName, bool? isHappy, string author = "Anonymous")
        {
            var targetLocation = locationService.GetLocation(locationName) ??
                throw new ArgumentException(locationName == null ? Constants.LocationNotProvidedException :
                string.Format(Constants.LocationNotFoundException, locationName));

            var comment = new Comment(author, commentText, targetLocation, isHappy);
            this.dBContext.Comments.Add(comment);
            this.dBContext.SaveChanges();

            return comment;
        }

        public IQueryable<Comment> GetAllComments()
        {
            return this.dBContext.Comments.Include(x => x.Location).OrderByDescending(x => x.CreatedOn);
        }

        public Comment EditComment(string commentId, string updatedText)
        {
            var commentForUpdate = GetCommentById(commentId);

            commentForUpdate.Text = updatedText;
            this.dBContext.SaveChanges();

            return commentForUpdate;
        }

        public void DeleteComment(string commentId)
        {
            var commentToDelete = GetCommentById(commentId);
            this.dBContext.Comments.Remove(commentToDelete);
            this.dBContext.SaveChanges();
        }

        public Comment GetCommentById(string commentId)
        {
            return this.dBContext.Comments.Include(x => x.Location).FirstOrDefault(x => x.Id == commentId) ??
                throw new ArgumentNullException(Constants.CommentNotFoundException);
        }
    }
}
