﻿using FourthCircle.Data.Entities;
using FourthCircle.Mappers;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FourthCircle.Areas.Management.Controllers
{
    public class LogbooksDropDownList : ViewComponent
    {
        private readonly ILogbookService logbookService;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly LogbookViewModelMapper logbookMapper;

        public LogbooksDropDownList(ILogbookService logbookService, UserManager<ApplicationUser> userManager, LogbookViewModelMapper logbookMapper)
        {
            this.logbookService = logbookService;
            this.userManager = userManager;
            this.logbookMapper = logbookMapper;
        }

        public Task<IViewComponentResult> InvokeAsync()
        {
            string userId = userManager.GetUserId(UserClaimsPrincipal);
            if (userId == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }

            var vm = logbookService.GetLogbooksByUser(userId).Select(logbookMapper.MapFrom);

            var result = (IViewComponentResult)View(vm);
            return Task.FromResult(result);
        }
    }
}
