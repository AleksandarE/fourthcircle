﻿using Exceptionless;
using FourthCircle.Data.Entities;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.IO;
using System.Linq;
using X.PagedList;

namespace FourthCircle.Areas.Management.Controllers
{
    [Area("Management")]
    [Authorize(Roles = "Manager")]
    public class LogbooksController : Controller
    {
        private readonly ILogbookService logbookService;
        private readonly ILocationService locationService;
        private readonly IManagerService managerService;
        private readonly LogbookViewModelMapper logbookMapper;
        private readonly LocationViewModelMapper locationMapper;
        private readonly NoteViewModelMapper noteMapper;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ICommentService commentService;
        private readonly IUserService userService;

        public LogbooksController(ILogbookService logbookService, LogbookViewModelMapper logbookMapper, NoteViewModelMapper noteMapper, IManagerService managerService,
            UserManager<ApplicationUser> userManager, ILocationService locationService, LocationViewModelMapper locationMapper, ICommentService commentService, IUserService userService)
        {
            this.locationService = locationService;
            this.logbookService = logbookService;
            this.logbookMapper = logbookMapper;
            this.locationMapper = locationMapper;
            this.managerService = managerService;
            this.userManager = userManager;
            this.noteMapper = noteMapper;
            this.commentService = commentService;
            this.userService = userService;
        }

        public IActionResult Index()
        {
            var userId = userManager.GetUserId(User);
            if (userId == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }

            var vm = locationService.GetLocationsByUser(userId).Select(locationMapper.MapFrom);

            return View(vm);
        }

        public IActionResult Notes(string Id, int? page, string searchString, string category, DateTime? dateFrom, DateTime? dateTo, bool showAll)
        {
            IPagedList<NoteViewModel> vm;
            try
            {
                if (showAll == true)
                {
                    vm = managerService.FilterLogbookNotes(Id, category, searchString, dateFrom, dateTo)
                    .Select(noteMapper.MapFrom)
                    .ToPagedList(page ?? 1, 7);
                }
                else
                {
                    vm = managerService.FilterLogbookNotes(Id, category, searchString, dateFrom, dateTo).Where(x => x.IsMarkedAsSolved == false)
                    .Select(noteMapper.MapFrom)
                    .ToPagedList(page ?? 1, 7);
                }
            }
            catch (Exception)
            {
                ExceptionlessClient.Default.SubmitNotFound($"/Management/Logbooks/Notes/{Id}");
                return BadRequest();
            }

            ViewBag.logbookId = Id;
            ViewBag.categories = GetCategoryViewBagByLogbook(Id);
            ViewBag.category = category;
            ViewBag.searchString = searchString;
            ViewBag.dateFrom = dateFrom;
            ViewBag.dateTo = dateTo;
            ViewBag.showAll = showAll;
            return View(vm);
        }

        public IActionResult MarkNoteAsSolved(string id, string logbookId, int? page, string searchString, string category, DateTime? dateFrom, DateTime? dateTo, bool showAll)
        {
            var userId = userManager.GetUserId(User);

            try
            {
                managerService.MarkNoteAsSolved(userId, id, logbookId);
            }
            catch (Exception)
            {
                ExceptionlessClient.Default.SubmitNotFound($"/Management/Logbooks/MarkNoteAsSolved?logbookId={logbookId}&noteId={id}");
                return BadRequest();
            }

            IPagedList<NoteViewModel> vm;
            if (showAll == true)
            {
                vm = managerService.FilterLogbookNotes(logbookId, category, searchString, dateFrom, dateTo)
                .Select(noteMapper.MapFrom)
                .ToPagedList(page ?? 1, 7);
            }
            else
            {
                vm = managerService.FilterLogbookNotes(logbookId, category, searchString, dateFrom, dateTo).Where(x => x.IsMarkedAsSolved == false)
                .Select(noteMapper.MapFrom)
                .ToPagedList(page ?? 1, 7);
            }

            ViewBag.logbookId = logbookId;
            ViewBag.categories = GetCategoryViewBagByLogbook(logbookId);
            ViewBag.category = category;
            ViewBag.searchString = searchString;
            ViewBag.dateFrom = dateFrom;
            ViewBag.dateTo = dateTo;
            ViewBag.showAll = showAll;
            return View("Notes", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<IActionResult> AddNoteAsync(NoteViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("AddNote", model);
            }

            try
            {
                var userId = userManager.GetUserId(User);
                if (userId == null)
                {
                    throw new ApplicationException($"Unable to load user with ID '{userId}'.");
                }

                managerService.AddNote(
                        userId, model.Text, model.LogbookID, model.Categories, model.UploadFormPicture);
                var state = ModelState.IsValid;
                return RedirectToAction("Notes", new { Id = model.LogbookID });
            }
            catch (ArgumentException ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("AddNote", new NoteViewModel { Id = model.LogbookID });
            }
        }

        [HttpGet]
        public IActionResult EditNote(string logbookId, string noteId)
        {
            var userId = userManager.GetUserId(User);
            if (userId == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }

            ViewBag.categories = GetCategoryViewBagByLogbook(logbookId);

            var vm = noteMapper.MapFrom(managerService.GetNoteById(noteId), userId, logbookId);

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditNote(NoteViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                managerService.EditNote(
                    model.UserID, model.Id, model.LogbookID, model.Text, model.Categories);
                return RedirectToAction("Notes", new { Id = model.LogbookID });
            }
            catch (ArgumentException ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View(new NoteViewModel { Id = model.LogbookID });
            }
        }

        public IActionResult DeleteNote(string logbookId, string noteId)
        {
            var userId = userManager.GetUserId(User);
            if (userId == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }

            ViewBag.categories = GetCategoryViewBagByLogbook(logbookId);

            try
            {
                managerService.DeleteNote(userId, noteId, logbookId);
                return RedirectToAction("Notes", new { Id = logbookId });
            }
            catch (Exception)
            {
                ExceptionlessClient.Default.SubmitNotFound($"/Management/Logbooks/DeleteNote/?logbookId={logbookId}&noteId={noteId}");
                return BadRequest();
            }
        }

        [HttpGet]
        public IActionResult AddCommentAsNote(string commentId)
        {
            var userId = userManager.GetUserId(User);
            if (userId == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }

            ViewBag.logbooks = logbookService.GetLogbooksByUser(userId).Select(logbookMapper.MapFrom);

            var comment = commentService.GetCommentById(commentId);

            var model = new NoteViewModel
            {
                UserID = userId,
                Text = comment.Text.Insert(0, $"Customer comment from \"{comment.Author}\":\n")
            };

            return PartialView("AddCommentAsNote", model);
        }

        public JsonResult CategoryDropDownList(string logbookId)
        {
            var categories = managerService.GetLogbookCategories(logbookId);

            //db.PageStats.Where(x => x.Country.Equals(country)).OrderBy(x => x.City).Select(x => x.City).Distinct().ToList();

            //ViewBag.categories = GetCategoryViewBagByLogbook(Id);

            //List<SelectListItem> cities = new List<SelectListItem>();

            //foreach (var item in results)
            //{
            //    SelectListItem city = new SelectListItem
            //    {
            //        Text = item,
            //        Value = item
            //    };
            //    cities.Add(city);
            //}
            return Json(categories);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Consumes("multipart/form-data")]
        public async System.Threading.Tasks.Task<IActionResult> AddCommentAsNote(NoteViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("AddNote", model);
            }

            try
            {
                // TODO: move this logic into the service

                if (model.UploadFormPicture != null)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await model.UploadFormPicture.CopyToAsync(memoryStream);
                        model.Picture = memoryStream.ToArray();
                    }
                }

                managerService.AddCommentAsNote(
                        model.UserID, model.Text, model.LogbookID, model.Categories, model.Picture);
                var state = ModelState.IsValid;
                return RedirectToAction("Notes", new { Id = model.LogbookID });
            }
            catch (ArgumentException ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View(new NoteViewModel { Id = model.LogbookID });
            }
        }

        private SelectList GetCategoryViewBagByLogbook(string logbookId)
        {
            var CategoryLst = managerService.GetCategoryListAsString(logbookId);
            return new SelectList(CategoryLst);
        }
    }
}