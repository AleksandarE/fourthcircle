﻿using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FourthCircle.Areas.Administrative.Controllers
{
    [Area("Administrative")]
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {
        private readonly IUserService userService;

        public HomeController(
            IUserService userService)
        {
            this.userService = userService;
        }

        // GET: /<controller>/
        [HttpGet]
        public IActionResult Index()
        {
            ViewData["Users"] = userService.GetAllUsers().Count();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(string url)
        {
            return RedirectToAction();
        }
    }
}
