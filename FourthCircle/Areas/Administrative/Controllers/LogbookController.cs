﻿using Exceptionless;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace FourthCircle.Areas.Administrative.Controllers
{
    [Area("Administrative")]
    [Authorize(Roles = "Admin")]
    public class LogbookController : Controller
    {
        private readonly ILogbookService logbookService;
        private readonly LogbookViewModelMapper mapper;
        private readonly ICategoryService categoryService;
        private readonly IUserService userService;

        public LogbookController(
            ILogbookService logbookService,
            LogbookViewModelMapper mapper,
            ICategoryService categoryService,
            IUserService userService)
        {
            this.logbookService = logbookService;
            this.mapper = mapper;
            this.categoryService = categoryService;
            this.userService = userService;
        }

        public async Task<IActionResult> ListLogbooks(string sortOrder, string currentFilter, string searchString, string currentUserFilter, string userSearch, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["LocationSortParm"] = string.IsNullOrEmpty(sortOrder) ? "location_desc" : "";
            ViewData["LogbookSortParm"] = sortOrder == "logbook_desc" ? "logbook_name" : "logbook_desc";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            if (userSearch != null)
            {
                pageNumber = 1;
            }
            else
            {
                userSearch = currentUserFilter;
            }

            ViewData["SearchString"] = searchString;
            ViewData["UserSearch"] = userSearch;

            var logbooks = logbookService.FilterLogbooks(sortOrder, searchString, userSearch);

            int pageSize = 5;

            var viewModels = await logbooks.Select(mapper.MapFrom).ToPagedListAsync(pageNumber ?? 1, pageSize);
            return View(viewModels);
        }

        [HttpGet]
        public IActionResult CreateCategory()
        {
            return PartialView("CreateCategory");
        }


        [HttpPost]
        public IActionResult CreateCategory(CategoryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("CreateCategory", model);
            }

            try
            {
                this.categoryService.CreateCategory(model.Name);
                return View("CreateCategory", model);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitNotFound($"/Administrative/Logbook/CreateCategory");
                ModelState.AddModelError(string.Empty, $"{ex.Message}");
                return View("CreateCategory", model);
            }
        }

        [HttpGet]
        public async Task<IActionResult> CreateLogbook()
        {
            ViewBag.CategoriesToSelect = GetCategories();
            ViewBag.ManagersToSelect = await GetManagers();

            return PartialView("CreateLogbook");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateLogbook(LogbookViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.CategoriesToSelect = GetCategories();
                ViewBag.ManagersToSelect = await GetManagers();
                return View("CreateLogbook", model);
            }

            try
            {
                var logbook = this.logbookService.CreateLogbook(model.Name, model.LocationName, model.Categories, model.AssignedManagers);
                return View("CreateLogbook", model);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitNotFound($"/Administrative/Logbook/CreateLogbook");
                ViewBag.CategoriesToSelect = GetCategories();
                ViewBag.ManagersToSelect = await GetManagers();
                ModelState.AddModelError(string.Empty, $"{ex.Message}");
                return View("CreateLogbook", model);
            }
        }

        public async Task<IActionResult> Details(string Id)
        {
            var targetLogbook = logbookService.GetLogbook(Id);

            if (targetLogbook == null)
            {
                ExceptionlessClient.Default.SubmitNotFound($"/Administrative/Logbook/Details/{Id}");
                return BadRequest();
            }

            var vm = mapper.MapFrom(targetLogbook);
            ViewBag.CategoriesToSelect = GetCategories();
            ViewBag.ManagersToSelect = await GetManagers();
            return PartialView(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateCategories(LogbookViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Details), model);
            }

            var updatedLogbook = this.logbookService.AssignCategories(model.Id, model.Categories);
            return RedirectToAction("ListLogbooks", "Logbook");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateManagers(LogbookViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Details), model);
            }

            var updatedLogbook = this.logbookService.AssignManagers(model.Id, model.AssignedManagers);
            return RedirectToAction("ListLogbooks", "Logbook");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteLogbook(LogbookViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Details), model);
            }

            this.logbookService.DeleteLogbook(logbookService.GetLogbook(model.Id));
            return RedirectToAction("ListLogbooks", "Logbook");
        }

        private IEnumerable<SelectListItem> GetCategories()
        {
            var categoryNames = categoryService.GetAllCategories();
            var output = categoryNames.Select(r => new SelectListItem { Value = r.Name, Text = r.Name }).OrderBy(x => x.Value);
            return output;
        }

        private async Task<IEnumerable<SelectListItem>> GetManagers()
        {
            var managers = await userService.GetUsernamesInRole("Manager");
            var output = managers.Select(manager => new SelectListItem { Value = manager, Text = manager }).OrderBy(x => x.Value);
            return output;
        }
    }
}
