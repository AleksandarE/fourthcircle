﻿using Exceptionless;
using FourthCircle.Data.Entities;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FourthCircle.Areas.Administrative.Controllers
{
    [Area("Administrative")]
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private readonly UserViewModelMapper mapper;
        private readonly IUserService userService;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<ApplicationUser> userManager;

        public UserController(
            UserViewModelMapper mapper,
            IUserService userService,
            RoleManager<IdentityRole> roleManager,
            UserManager<ApplicationUser> userManager)
        {
            this.mapper = mapper;
            this.userService = userService;
            this.roleManager = roleManager;
            this.userManager = userManager;
        }

        [Route("Administrative/Users")]
        public async Task<IActionResult> ListUsers(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["SearchString"] = searchString;

            var users = userService.FilterUsers(sortOrder, searchString);
            int pageSize = 5;
            IPagedList<UserViewModel> userVMs = await users.Select(mapper.MapFrom).ToPagedListAsync(pageNumber ?? 1, pageSize);

            return View(userVMs);
        }

        public IActionResult Details(string Id)
        {
            var targetUser = userService.GetUserById(Id);
            if (targetUser == null)
            {
                ExceptionlessClient.Default.SubmitNotFound($"/Administrative/Users/Details/{Id}");
                return BadRequest();
            }

            var vm = this.mapper.MapFrom(targetUser);
            ViewBag.Roles = userService.GetAvailableRoles();

            return PartialView(vm);
        }

        [Route("Administrative/User/DeleteUser/{id}")]
        public IActionResult DeleteUser(UserViewModel model)
        {
            if (!ModelState.IsValid) return RedirectToAction("Details", new { Id = model.Id });

            userService.DeleteUser(model.Id);

            return RedirectToAction("ListUsers");
        }

        [Route("Administrative/User/UpdateRole/{id}")]
        public async Task<IActionResult> UpdateRole(UserViewModel model)
        {
            if (!ModelState.IsValid) return RedirectToAction("Details", new { Id = model.Id });

            var output = await userService.ChangeRole(model.Id, model.UpdatableRole);

            return RedirectToAction("ListUsers");
        }

        private List<SelectListItem> GetAvailableRoles()
        {
            List<SelectListItem> availableRoles = new List<SelectListItem>();
            foreach (var item in roleManager.Roles)
            {
                availableRoles.Add(new SelectListItem { Value = item.Name, Text = item.Name });
            }
            return availableRoles;
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}