﻿using Exceptionless;
using FourthCircle.Mappers;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FourthCircle.Areas.Administrative.Controllers
{
    [Area("Administrative")]
    [Authorize(Roles = "Moderator")]
    public class ModeratorController : Controller
    {
        private readonly ICommentService commentService;
        private readonly CommentViewModelMapper mapper;

        public ModeratorController(ICommentService commentService, CommentViewModelMapper mapper)
        {
            this.commentService = commentService;
            this.mapper = mapper;
        }
        [HttpGet]
        public IActionResult Details(string Id)
        {
            var targetComment = commentService.GetCommentById(Id);
            if (targetComment == null)
            {
                ExceptionlessClient.Default.SubmitNotFound($"/Administrative/Moderator/Details/{Id}");
                return BadRequest();
            }
            var vm = this.mapper.MapFrom(targetComment);
            return PartialView("UpdateComment", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditComment(CommentViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Details", "Moderator", new { area = "Administrative", Id = model.Id });
            }

            var updatedComment = commentService.EditComment(model.Id, model.Text);
            return RedirectToAction("Index", "Home", new { area = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteComment(string Id)
        {
            var targetComment = commentService.GetCommentById(Id);
            if (targetComment == null)
            {
                ExceptionlessClient.Default.SubmitNotFound($"/Administrative/Moderator/Details/{Id}");
                return BadRequest();
            }

            commentService.DeleteComment(Id);
            return RedirectToAction("Index", "Home", new { area = "" });
        }
    }
}
