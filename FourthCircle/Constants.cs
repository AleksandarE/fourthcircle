﻿namespace FourthCircle
{
    public static class Constants
    {
        public const string DefaultGuestName = "Anonymous";

        public const string CommentLengthErrorMessage = "Please enter text between 5 and 280 characters.";
        public const string UnselectedLocationErrorMessage = "Please specify the location";


    }
}
