﻿namespace FourthCircle.Mappers.Interfaces
{
    public interface IViewModelMapper<TEntity, TViewModel>
    {
        TViewModel MapFrom(TEntity entity);
    }
}
