﻿using FourthCircle.Data.Entities;
using FourthCircle.Mappers.Interfaces;
using FourthCircle.Models.EntityViewModels;

namespace FourthCircle.Mappers
{
    public class LocationViewModelMapper : IViewModelMapper<Location, LocationViewModel>
    {
        public LocationViewModelMapper()
        {
        }

        public LocationViewModel MapFrom(Location entity)
        {
            var vm = new LocationViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Lattitude = entity.Lattitude,
                Longtitude = entity.Longtitude,
                Logbooks = entity.Logbooks
            };

            return vm;
        }
    }
}
