﻿using FourthCircle.Data.Entities;
using FourthCircle.Mappers.Interfaces;
using FourthCircle.Models.EntityViewModels;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace FourthCircle.Mappers
{
    public class UserViewModelMapper : IViewModelMapper<ApplicationUser, UserViewModel>
    {
        private readonly UserManager<ApplicationUser> userManager;

        public UserViewModelMapper(UserManager<ApplicationUser> userManager)
        {
            this.userManager = userManager;
        }

        public UserViewModel MapFrom(ApplicationUser entity)
        {
            var vm = new UserViewModel
            {
                Id = entity.Id,
                UserName = entity.UserName,
                Email = entity.Email,
                AssignedRole = userManager.GetRolesAsync(entity).Result.FirstOrDefault(),
                UpdatableRole = ""
            };

            return vm;
        }
    }
}