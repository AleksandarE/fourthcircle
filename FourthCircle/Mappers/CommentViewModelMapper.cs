﻿
using FourthCircle.Data.Entities;
using FourthCircle.Mappers.Interfaces;
using FourthCircle.Models.EntityViewModels;

namespace FourthCircle.Mappers
{
    public class CommentViewModelMapper : IViewModelMapper<Comment, CommentViewModel>
    {
        public CommentViewModel MapFrom(Comment entity)
        {
            var vm = new CommentViewModel
            {
                Id = entity.Id,
                Text = entity.Text,
                Author = entity.Author,
                LocationName = entity.Location.Name,
                CreatedOn = entity.CreatedOn.ToShortDateString(),
                IsHappy = entity.IsHappy
            };
            return vm;
        }
    }
}
