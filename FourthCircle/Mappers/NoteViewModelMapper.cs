﻿using FourthCircle.Data.Entities;
using FourthCircle.Mappers.Interfaces;
using FourthCircle.Models.EntityViewModels;

namespace FourthCircle.Mappers
{
    public class NoteViewModelMapper : IViewModelMapper<Note, NoteViewModel>
    {
        public NoteViewModelMapper()
        {
        }

        public NoteViewModel MapFrom(Note entity)
        {
            var vm = new NoteViewModel
            {
                Id = entity.Id,
                Text = entity.Text,
                UserID = entity.UserId,
                Picture = entity.Picture,
                LogbookID = entity.LogbookId,
                CreatedOn = entity.CreatedOn.ToString("dd/MM/yyyy HH:mm"),
                UserAvatar = entity.User.Avatar,
                UserName = entity.User.UserName,
                IsMarkedAsSolved = entity.IsMarkedAsSolved,
                SolvedByUser = entity.SolvedByUser
            };

            if (entity.NotesCategories != null)
            {
                foreach (var notesCategories in entity.NotesCategories)
                {
                    vm.Categories.Add(notesCategories.Category.Name);
                }
            }

            return vm;
        }

        public NoteViewModel MapFrom(Note entity, string userId, string logbookId)
        {
            var vm = new NoteViewModel
            {
                Id = entity.Id,
                Text = entity.Text,
                UserID = userId,
                Picture = entity.Picture,
                LogbookID = logbookId,
                CreatedOn = entity.CreatedOn.ToString("MM/dd/yyyy HH:mm"),
                IsMarkedAsSolved = entity.IsMarkedAsSolved
            };

            if (entity.NotesCategories != null)
            {
                foreach (var notesCategories in entity.NotesCategories)
                {
                    vm.Categories.Add(notesCategories.Category.Name);
                }
            }

            return vm;
        }
    }
}
