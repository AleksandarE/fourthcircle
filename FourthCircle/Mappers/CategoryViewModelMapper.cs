﻿using FourthCircle.Data.Entities;
using FourthCircle.Mappers.Interfaces;
using FourthCircle.Models.EntityViewModels;

namespace FourthCircle.Mappers
{
    public class CategoryViewModelMapper : IViewModelMapper<Category, CategoryViewModel>
    {
        public CategoryViewModelMapper()
        {
        }

        public CategoryViewModel MapFrom(Category entity)
        {
            var vm = new CategoryViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
            };

            return vm;
        }
    }
}
