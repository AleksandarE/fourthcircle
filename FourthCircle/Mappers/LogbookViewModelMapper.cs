﻿using FourthCircle.Data.Entities;
using FourthCircle.Mappers.Interfaces;
using FourthCircle.Models.EntityViewModels;

namespace FourthCircle.Mappers
{
    public class LogbookViewModelMapper : IViewModelMapper<Logbook, LogbookViewModel>
    {
        public LogbookViewModelMapper()
        {
        }

        public LogbookViewModel MapFrom(Logbook entity)
        {
            var vm = new LogbookViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                LocationName = entity.Location.Name,
                Notes = entity.Notes,
            };

            foreach (var item in entity.LogbookCategories)
            {
                vm.Categories.Add(item.Category.Name);
            }

            foreach (var item in entity.LogbooksUsers)
            {
                vm.AssignedManagers.Add(item.User.UserName);
            }
            return vm;
        }
    }
}
