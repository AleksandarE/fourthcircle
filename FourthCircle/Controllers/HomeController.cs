﻿using Exceptionless;
using FourthCircle.Mappers;
using FourthCircle.Models;
using FourthCircle.Models.EntityViewModels;
using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace FourthCircle.Controllers
{
    public class HomeController : Controller
    {
        private readonly CommentViewModelMapper mapper;
        private readonly ICommentService visitorService;
        private readonly ILocationService locationService;

        public HomeController(CommentViewModelMapper mapper, ICommentService visitorService, ILocationService locationService)
        {
            this.mapper = mapper;
            this.visitorService = visitorService;
            this.locationService = locationService;
        }

        public async Task<IActionResult> Index(int? pageNumber)
        {
            var comments = this.visitorService.GetAllComments();
            var pageSize = 10;
            var models = await comments.Select(mapper.MapFrom).ToPagedListAsync(pageNumber ?? 1, pageSize);

            return View(models);
        }

        public IActionResult About()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult CreateComment()
        {
            var locations = this.locationService.GetLocationNames();
            ViewBag.AvailableLocations = locations;
            return PartialView("CreateComment");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateComment(CommentViewModel model)
        {
            var locations = this.locationService.GetLocationNames();

            if (!ModelState.IsValid)
            {
                ViewBag.AvailableLocations = locations;
                return BadRequest(this.ModelState);
            }

            try
            {
                if (model.Author == null)
                {
                    model.Author = "Anonymous";
                }
                ViewBag.AvailableLocations = locations;
                var comment = this.visitorService.CreateComment(model.Text, model.LocationName, model.IsHappy, model.Author);

                return PartialView("_CommentPartial", model);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitNotFound($"/CreateComment");
                return BadRequest(ex.Message);
            }
        }
    }
}
