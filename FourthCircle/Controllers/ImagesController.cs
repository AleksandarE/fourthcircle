﻿using FourthCircle.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace FourthCircle.Controllers
{
    public class ImagesController : Controller
    {
        readonly IUserService userService;
        readonly IManagerService managerService;

        public ImagesController(IUserService userService, IManagerService managerService)
        {
            this.userService = userService;
            this.managerService = managerService;
        }

        public IActionResult GetUserAvatar(string userId)
        {
            byte[] image = userService.GetUserAvatar(userId);
            return File(image, "image/png");
        }

        public IActionResult GetUploadImage(string noteId)
        {
            byte[] image = managerService.GetNoteById(noteId).Picture;
            return File(image, "image/JPEG");
        }
    }
}