﻿$(function () {
    var placeholderElement = $('#placeholder');

    $('button[data-toggle="ajax-modal"]').click(function (event) {
        var url = $(this).data('url');
        $.get(url).done(function (data) {
            placeholderElement.html(data);
            placeholderElement.find('.modal').modal('show');
        });
    });

    placeholderElement.on('click', '[data-save="modal"]', function (event) {
        event.preventDefault();
        var form = $(this).parents('.modal').find('form');
        var actionUrl = form.attr('action');
        var dataToSend = form.serialize();

        $.post(actionUrl, dataToSend)
            .done(function (data) {
                console.log(data);
                
                $('.comments-container').prepend(data);
                $('.comment-text').profanityFilter({
                    externalSwears: '/swearWords.json'
                });
                    placeholderElement.find('.modal').modal('hide');
                    toastr.success("Thank you fo your comment");
            })
            .fail(function(data) {
                console.log(data)
                console.log('shit happened')
                var errors = data.responseJSON;
                console.log(errors)

                $.each(errors, function (key, value) {
                    toastr.error(value);
                })
                toastr.options = {
                      "closeButton": false,
                      "debug": false,
                      "newestOnTop": true,
                      "progressBar": false,
                      "positionClass": "toast-top-center",
                      "preventDuplicates": true,
                      "onclick": null,
                      "showDuration": "300",
                      "hideDuration": "1000",
                      "timeOut": "5000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                      }
        })
    }); 
});
