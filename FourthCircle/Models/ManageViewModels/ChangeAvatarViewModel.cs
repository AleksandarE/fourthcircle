﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace FourthCircle.Models.ManageViewModels
{
    public class ChangeAvatarViewModel
    {
        [Display(Name = "Current avatar")]
        public byte[] OldAvatar { get; set; }

        [Required]
        [Display(Name = "New avatar")]
        public IFormFile NewAvatar { get; set; }

        public string StatusMessage { get; set; }
    }
}
