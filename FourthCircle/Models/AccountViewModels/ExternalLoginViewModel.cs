using System.ComponentModel.DataAnnotations;

namespace FourthCircle.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
