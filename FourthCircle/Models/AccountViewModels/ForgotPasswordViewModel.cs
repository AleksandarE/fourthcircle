﻿using System.ComponentModel.DataAnnotations;

namespace FourthCircle.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
