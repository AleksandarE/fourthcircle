﻿using FourthCircle.Data.Entities;
using System.Collections.Generic;

namespace FourthCircle.Models.EntityViewModels
{
    public class LocationViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Lattitude { get; set; }
        public string Longtitude { get; set; }
        public ICollection<Logbook> Logbooks { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
