﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FourthCircle.Models.EntityViewModels
{
    public class CommentViewModel
    {
        public CommentViewModel()
        {
        }
        public string Id { get; set; }
        [Required]
        [StringLength(280, MinimumLength = 5, ErrorMessage = Constants.CommentLengthErrorMessage)]
        public string Text { get; set; }
        [DefaultValue(Constants.DefaultGuestName)]
        public string Author { get; set; } = Constants.DefaultGuestName;
        [Required(ErrorMessage = Constants.UnselectedLocationErrorMessage)]
        public string LocationName { get; set; }
        public string CreatedOn { get; set; }
        public bool? IsHappy { get; set; }
    }
}
