﻿namespace FourthCircle.Models.EntityViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {

        }

        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string AssignedRole { get; set; }
        public string UpdatableRole { get; set; }

    }
}