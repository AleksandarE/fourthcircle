﻿using FourthCircle.Data.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FourthCircle.Models.EntityViewModels
{
    public class LogbookViewModel
    {
        public LogbookViewModel()
        {
            Categories = new List<string>();
            AssignedManagers = new List<string>();
            Notes = new List<Note>();
            Comments = new List<Comment>();
        }

        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string LocationName { get; set; }
        public ICollection<string> Categories { get; set; }
        public ICollection<string> AssignedManagers { get; set; }
        public ICollection<Note> Notes { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
