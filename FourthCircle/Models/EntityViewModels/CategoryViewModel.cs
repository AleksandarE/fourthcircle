﻿using System.ComponentModel.DataAnnotations;

namespace FourthCircle.Models.EntityViewModels
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {

        }
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
