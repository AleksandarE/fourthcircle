﻿using FourthCircle.Data.Entities;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FourthCircle.Models.EntityViewModels
{
    public class NoteViewModel
    {
        public NoteViewModel()
        {
            Categories = new List<string>();
        }

        public string Id { get; set; }
        [Required]
        [StringLength(280, MinimumLength = 5, ErrorMessage = "Please enter text between 5 and 280 characters.")]
        public string Text { get; set; }
        public ICollection<string> Categories { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public byte[] UserAvatar { get; set; }
        public bool IsMarkedAsSolved { get; set; }
        public ApplicationUser SolvedByUser { get; set; }
        public bool AddAsComment { get; set; }
        public byte[] Picture { get; set; }
        public IFormFile UploadFormPicture { get; set; }
        public string LogbookID { get; set; }
        public Logbook Logbook { get; set; }
        public string CreatedOn { get; set; }
    }
}
