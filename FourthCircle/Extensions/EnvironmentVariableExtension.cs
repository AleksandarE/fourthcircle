﻿using Microsoft.Extensions.Configuration;
using System;
using System.Runtime.InteropServices;

namespace FourthCircle.Extensions
{
    public static class EnvironmentVariableExtensions
    {
        public static string GetConnectionStringFromEnvironment(this IConfiguration configuration)
        {
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return Environment.GetEnvironmentVariable("FourthCircleConnection");

            return Environment.GetEnvironmentVariable("FourthCircleConnection", EnvironmentVariableTarget.User);
        }
    }
}